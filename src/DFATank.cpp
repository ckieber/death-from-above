#include "DFAAvatar.h"
#include "DFAPhysicsManager.h"
#include "DFATank.h"
#include "DFAWorldData.h"
#include "OgreOggSound.h"

#define CAMERA_DIST 30
#define CAMERA_HEIGHT 10
#define CAMERA_LOOK_AT 20

#define MAX_TARGET_RANGE 1500
#define CLOSEST_DISTANCE_TO_TARGET 500
#define SHOOT_TIME_DELAY 10

//#define MAX_MOVE_FORCE 350000
#define MAX_SPEED 1000

using namespace Ogre;

DFAAI::DFATank::DFATank() : NxOgre::Callback()
{
	mHealth = 5000;
	mCurrentWeapon = 0;
	mDestroyed = false;
	mPlayDestroyedAnimation = false;
	mTimeOfLastShot = 0;
}

DFAAI::DFATank::~DFATank()
{
	if(mDotSceneNode)
		mDotSceneNode->Destroy();

	if(mWeaponContainer)
		delete mWeaponContainer;

	if(mCameraNode)
		delete mCameraNode;
	if(mObjectNode)
		delete mObjectNode;
	if(mMovementNode)
		delete mMovementNode;

	if(mCollisionTool)
		delete mCollisionTool;

	if(mActor)
		DFAData::DFAPhysicsManager::getSingletonPtr()->getScene()->destroyActor(mActor);
}

void DFAAI::DFATank::loadAnimations(Ogre::SceneManager *sceneManager)
{
	
}

void DFAAI::DFATank::loadSounds()
{
	
}

void DFAAI::DFATank::updateAnimations(const Ogre::Real frameRate)
{
	
}

void DFAAI::DFATank::createObject(const Ogre::String name, const int id, const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
								  OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager, const DFACore::QueryMask queryMask)
{
	mCollisionTool = new DFATools::DFACollisionTool(sceneManager);
	mId = id;
	mNamePrefix = StringConverter::toString(id) + "_";
	mName = name;
	mQueryMask = queryMask;
	mDotSceneNode = new OgreMax::OgreMaxScene();
	mDotSceneNode->SetNamePrefix(mNamePrefix);
	mDotSceneNode->Load(sceneName, renderWindow, loadOptions, sceneManager, sceneManager->getRootSceneNode()->createChildSceneNode(mNamePrefix + "TankMovementNode")->createChildSceneNode(mNamePrefix + "TankNode"));

	mMovementNode = sceneManager->getSceneNode(mNamePrefix + "TankMovementNode");
	mObjectNode = sceneManager->getSceneNode(mNamePrefix + "TankNode");
	mCameraNode = mMovementNode->createChildSceneNode(mNamePrefix + "TankCameraNode");

	//load weapons
	Ogre::uint32 targetFlags = DFACore::AVATAR | DFACore::STATIONARY;
	mWeaponContainer = new DFACore::DFAWeaponContainer(sceneManager, DFACore::ROCKET_80MM, 0, targetFlags);

	//mMovementNode->setPosition(Vector3(500, 150, 500));

	//create physics
	NxOgre::RigidBodyDescription desc;
	desc.mName = "M1A2";
	desc.mMass = 69540;
	desc.mDynamicRigidbodyFlags |= NxOgre::DynamicRigidbodyFlags::FreezeRotation;
	desc.mContactReportFlags |= NxOgre::Enums::ContactPairFlags_All;
	//desc.mGroup = 1;
	NxOgre::BoxDescription boxDesc;
	boxDesc.mSize = Vector3(3.7, 2, 7);
	mActor = DFAData::DFAPhysicsManager::getSingletonPtr()->getScene()->createActor(boxDesc, NxOgre::Matrix44::IDENTITY, desc);
	mActor->setContactCallback(this);

	//set camera node position
	mCameraNode->setPosition(Vector3(0, CAMERA_HEIGHT, -CAMERA_DIST));
	mCameraNode->lookAt(Vector3(0, 0, CAMERA_LOOK_AT), Node::TS_PARENT);
	mCameraNode->roll(Degree(180));

	//Entity *ent = sceneManager->createEntity("Knot", "knot.mesh");
	SceneNode *moveNode = mObjectNode->createChildSceneNode(mNamePrefix + "TankMoveNode");
	moveNode->setPosition(Vector3(0, 0, 100));
	//moveNode->attachObject(ent);
	//moveNode->setScale(0.01, 0.01, 0.01);

	loadAnimations(sceneManager);
	loadSounds();
}

Vector3 DFAAI::DFATank::getPosition(void)
{
	return mMovementNode->getPosition();
}

Ogre::String DFAAI::DFATank::getNodeName(void)
{
	return mObjectNode->getName();
}

Ogre::SceneNode* DFAAI::DFATank::getCameraNode(void)
{
	return mCameraNode;
}

void DFAAI::DFATank::setPosition(const Ogre::Vector3 position)
{
	mActor->setCMassGlobalPosition(position);
	mMovementNode->setPosition(mActor->getGlobalPosition().as<Ogre::Vector3>());
}

void DFAAI::DFATank::update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager)
{
	updatePosition(frameRate);
	updateAnimations(frameRate);
}

void DFAAI::DFATank::updatePosition(const Ogre::Real frameRate)
{
	std::list<DFACore::DFAWorldObject*> worldObjects = DFAData::DFAWorldData::getSingletonPtr()->getWorldObjects();
	Vector3 avatarPosition = dynamic_cast<DFACore::DFAAvatar*>((*worldObjects.begin()))->getPosition();
	Vector3 tankPosition = getPosition();
	const Ogre::Real squaredTargetRange = Ogre::Math::Pow(MAX_TARGET_RANGE, 2);
	
	//check if the avatar is in target range
	Ogre::Real squaredDistance = avatarPosition.squaredDistance(tankPosition);
	if(squaredDistance < squaredTargetRange)
	{
		Vector3 targetDirection = avatarPosition - tankPosition;
		targetDirection.normalise();
		//avatar is in range -> shoot at it and move toward it
		time_t currentTime = time(NULL);
		if(currentTime - mTimeOfLastShot >= SHOOT_TIME_DELAY)
		{
			mTimeOfLastShot = currentTime;
			mWeaponContainer->shoot(tankPosition, targetDirection, avatarPosition);
		}

		//set the orientation
		Vector3 tankDirection = mObjectNode->getOrientation() * Vector3(0,0,-1);
		tankDirection.normalise(); tankDirection.y = 0;
		targetDirection.y = 0;
		Ogre::Quaternion rotationQuat = tankDirection.getRotationTo(targetDirection);
		mActor->setGlobalOrientationQuat(rotationQuat);
		mMovementNode->setOrientation(mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>());

		//move towards it if we're not too close
		if(squaredDistance >= Ogre::Math::Pow(CLOSEST_DISTANCE_TO_TARGET, 2))
		{
			//move to position
			Vector3 movePosition = (targetDirection + Vector3::NEGATIVE_UNIT_Y) * MAX_SPEED * frameRate;
			mActor->setLinearVelocity(movePosition);
		}
	}
	mMovementNode->setPosition(mActor->getGlobalPosition().as<Ogre::Vector3>());
}

OgreMax::OgreMaxScene::ObjectExtraDataMap& DFAAI::DFATank::getObjectExtraDataMap(void)
{
	return mDotSceneNode->GetAllObjectExtraData();
}

Ogre::String DFAAI::DFATank::getName(void)
{
	return mName;
}

DFACore::QueryMask DFAAI::DFATank::getQueryMask(void)
{
	return mQueryMask;
}

bool DFAAI::DFATank::isDestroyed(void)
{
	return mDestroyed;
}

void DFAAI::DFATank::decreaseHealth(const Ogre::Real damage)
{
	mHealth -= damage;
	if(mHealth <= 0)
	{
		mDestroyed = true;
		mActor->setContactCallback(NULL);
		//mPlayDestroyedAnimation = true;
	}
}

NxOgre::RigidBody* DFAAI::DFATank::getRigidBody(void)
{
	return mActor;
}

void DFAAI::DFATank::onContact(const NxOgre::ContactPair &pair)
{

}