#include "DFAPilotManager.h"

#define CRYPT 43
#define CRYPTSPACE 12

DFAData::DFAPilotManager *DFAData::DFAPilotManager::mPilotManager;

DFAData::DFAPilot* DFAData::DFAPilotManager::createPilot(const CEGUI::String &pilotName)
{
	if(!exists(pilotName))
	{
		DFAData::DFAPilot *newPilot = new DFAData::DFAPilot(pilotName);
		mPilots.push_back(newPilot);
		return newPilot;
	}
	else
		return getPilot(pilotName);
}

const CEGUI::String DFAData::DFAPilotManager::printPilot(const CEGUI::String &pilotName)
{
	DFAData::DFAPilot *pilot = getPilot(pilotName);
	if(pilot)
	{
		CEGUI::String output = "Pilot name: " + pilot->getName();
		output += "\nRank: " + pilot->getRank();
		output += "\nStatus: " + pilot->getStatus();
		output += "\nPlayed Missions: " + pilot->getPlayedMissions();
		output += "\nComplete Missions: " + pilot->getCompMissionsTotal();
		output += "\nComplete Day Missions: " + pilot->getCompMissionsDay();
		output += "\nComplete Night Missions: " + pilot->getCompMissionsNight();
		output += "\nIncomplete Missions: " + pilot->getIncompMissionsTotal();
		output += "\nIncomplete Day Missions: " + pilot->getIncompMissionsDay();
		output += "\nIncomplete Night Missions: " + pilot->getIncompMissionsNight();
		output += "\nKills: " + pilot->getKills();
		return output;
	}
	return "";
}

DFAData::DFAPilot* DFAData::DFAPilotManager::getPilot(const CEGUI::String &pilotName)
{
	std::list<DFAData::DFAPilot*>::iterator iter = mPilots.begin();
	for(; iter != mPilots.end(); iter++)
	{
		if((*iter)->getName() == pilotName)
			return *iter;
	}
	return NULL;
}

DFAData::DFAPilot* DFAData::DFAPilotManager::getActivePilot()
{
	if(mActivePilot)
		return mActivePilot;
	return NULL;
}

void DFAData::DFAPilotManager::setActivePilot(const CEGUI::String &pilotName)
{
	DFAData::DFAPilot *pilot = getPilot(pilotName);
	if(pilot)
		mActivePilot = pilot;
}

bool DFAData::DFAPilotManager::exists(const CEGUI::String &pilotName)
{
	std::list<DFAData::DFAPilot*>::iterator iter = mPilots.begin();
	for(; iter != mPilots.end(); iter++)
	{
		if((*iter)->getName() == pilotName)
			return true;
	}
	return false;
}

bool DFAData::DFAPilotManager::deletePilot(const CEGUI::String &pilotName)
{
	if(pilotName == "Default Pilot")
		return false;

	std::list<DFAData::DFAPilot*>::iterator iter = mPilots.begin();
	while(iter != mPilots.end() && (*iter)->getName() != pilotName)
	{
		iter++;
	}
	if(iter == mPilots.end())
		return false;

	mPilots.erase(iter);
	return true;

}

bool DFAData::DFAPilotManager::deleteAllPilots(void)
{
	std::list<DFAData::DFAPilot*>::iterator iter;
	while(mPilots.size() > 1)
	{
		iter = mPilots.begin();
		if((*iter)->getName() == "Default Pilot")
		{
			iter++;
			mPilots.erase(iter);
		}
		else
			mPilots.erase(iter);
	}
	
	return true;
}

void DFAData::DFAPilotManager::saveDataToFile(void)
{
	std::ofstream out;
	out.open(LOGFILE);

	std::list<DFAData::DFAPilot*>::iterator iter = mPilots.begin();
	//out << encryptInt(mPilots.size()) << std::endl;
	out << mPilots.size() << std::endl;
	for(; iter != mPilots.end(); iter++)
	{
		out << encryptString((*iter)->getName()) << std::endl;
		out << encryptString((*iter)->getRank()) << std::endl;
		out << encryptString((*iter)->getStatus()) << std::endl;
		out << encryptInt((*iter)->getScore()) << std::endl;
		out << encryptString((*iter)->getCompMissionsDay()) << std::endl;
		out << encryptString((*iter)->getCompMissionsNight()) << std::endl;
		out << encryptString((*iter)->getIncompMissionsDay()) << std::endl;
		out << encryptString((*iter)->getIncompMissionsNight()) << std::endl;
		out << encryptString((*iter)->getKills()) << std::endl;
	}

	out.close();
}

void DFAData::DFAPilotManager::loadDataFromFile(void)
{
	std::ifstream in;
	in.open(LOGFILE);

	if(!in.fail())
	{
		std::string name, rank, status;
		std::string temp, score, compMissionsDay, compMissionsNight, incompMissionsDay, incompMissionsNight, kills;
		int size = 0;
		DFAData::DFAPilot *pilot;
		//in >> temp;
		//size = decryptInt(temp);
		in >> size;
		while(size > 0)
		{
			in.ignore();
			getline(in, name);
			getline(in, rank);
			getline(in, status);
			in >> score;
			in >> compMissionsDay;
			in >> compMissionsNight;
			in >> incompMissionsDay;
			in >> incompMissionsNight;
			in >> kills;

			pilot = createPilot(decryptString(name));
			pilot->setRank(decryptString(rank));
			pilot->setStatus(decryptString(status));
			pilot->setScore(decryptInt(score));
			pilot->setCompMissionsDay(decryptInt(compMissionsDay));
			pilot->setCompMissionsNight(decryptInt(compMissionsNight));
			pilot->setIncompMissionsDay(decryptInt(incompMissionsDay));
			pilot->setIncompMissionsNight(decryptInt(incompMissionsNight));
			pilot->setKills(decryptInt(kills));

			size--;
		}
		in.close();
	}
	else
	{
		createPilot("Default Pilot");
		saveDataToFile();
	}
}

std::list<DFAData::DFAPilot*>* DFAData::DFAPilotManager::getPilotList(void)
{
	return &mPilots;
}

const CEGUI::String DFAData::DFAPilotManager::encryptString(CEGUI::String input)
{
	for(int i = 0; i<input.size(); i++)
	{
		if(input[i] != ' ')
			input[i] -= CRYPT;
		else
			input[i] -= CRYPTSPACE;
	}
	return input;
}

const CEGUI::String DFAData::DFAPilotManager::encryptInt(int input)
{
	std::stringstream s;
	s << input;
	CEGUI::String temp = (CEGUI::String)s.str();
	for(int i=0; i<temp.size(); i++)
	{
		if(temp[i] != ' ')
			temp[i] -= CRYPT;
		else
			temp[i] -= CRYPTSPACE;
	}

	return temp;
}

const CEGUI::String DFAData::DFAPilotManager::decryptString(CEGUI::String input)
{
	for(int i = 0; i<input.size(); i++)
	{
		if(input[i] != (' ' - CRYPTSPACE))
			input[i] += CRYPT;
		else
			input[i] += CRYPTSPACE;
	}
	return input;
}

const int DFAData::DFAPilotManager::decryptInt(CEGUI::String input)
{
	for(int i=0; i<input.size(); i++)
	{
		if(input[i] != (' ' - CRYPTSPACE))
			input[i] += CRYPT;
		else
			input[i] += CRYPTSPACE;
	}

	std::stringstream s;
	s << input;
	int value;
	s >> value;

	return value;
}

DFAData::DFAPilotManager* DFAData::DFAPilotManager::getSingletonPtr(void)
{
	if(!mPilotManager)
	{
		mPilotManager = new DFAData::DFAPilotManager();
	}

	return mPilotManager;
}