#include "DFAPhysicsManager.h"
#include "DFALaserGuidedMissile.h"
#include "DFAWorldData.h"
#include "OgreOggSound.h"

#define CAMERA_DIST 5
#define CAMERA_HEIGHT 2
#define CAMERA_LOOK_AT 1

#define MAX_RANGE 5000
#define MAX_SPEED 10//60000
#define DAMAGE 5000
#define MAX_TURN_ANGLE 0.01

#define ACTOR_POS_OFFSET_X 0
#define ACTOR_POS_OFFSET_Y 0.8
#define ACTOR_POS_OFFSET_Z 0

using namespace Ogre;

DFACore::DFALaserGuidedMissile::DFALaserGuidedMissile(const int id, const String &name, Ogre::SceneManager *sceneManager, const Ogre::uint32 queryMask) : DFACore::DFAMissile(queryMask)
{
	mCollisionTool = new DFATools::DFACollisionTool(sceneManager);
	mDestroyed = false;
	mPlayDestroyedAnimation = false;
	mId = id;
	mName = name;
	mSceneManager = sceneManager;
	mTargetFlags = 0xFFFFFFFF;
	
	mSpeed = MAX_SPEED;
	mDamage = DAMAGE;
	mDirection = Vector3::ZERO;
	mDistanceTraveled = 0;
	mPitchAngle = 0;//90;
	mYawAngle = 0;//180;
	mRollAngle = 0;

	//create missile object
	mNamePrefix = StringConverter::toString(id) + "_";
	mMovementNode = sceneManager->getRootSceneNode()->createChildSceneNode(mNamePrefix + "LaserGuidedMissileMovementNode");
	mObjectNode = mMovementNode->createChildSceneNode(mNamePrefix + "LaserGuidedMissileNode");
	mCameraNode = mMovementNode->createChildSceneNode(mNamePrefix + "LaserGuidedMissileCameraNode");
	Entity *missile = sceneManager->createEntity(mNamePrefix + mName, "S-8Rocket.mesh");
	missile->setQueryFlags(queryMask);
	mObjectNode->attachObject(missile);
	mObjectNode->scale(3.5, 3.5, 3.5);

	//create physics
	NxOgre::RigidBodyDescription desc;
	desc.mName = "AT-6Spiral";
	desc.mMass = 30;
	desc.mDynamicRigidbodyFlags |= NxOgre::DynamicRigidbodyFlags::DisableGravity;
	desc.mDynamicRigidbodyFlags |= NxOgre::DynamicRigidbodyFlags::FreezeRotation;
	desc.mContactReportFlags |= NxOgre::Enums::ContactPairFlags_All;
	NxOgre::BoxDescription boxDesc;
	boxDesc.mSize = Vector3(0.08, 1.40, 0.08);
	mActor = DFAData::DFAPhysicsManager::getSingletonPtr()->getScene()->createActor(boxDesc, NxOgre::Matrix44::IDENTITY, desc);
	mActor->setContactCallback(this);

	//set camera node position
	mCameraNode->setPosition(Vector3(0, CAMERA_HEIGHT, -CAMERA_DIST));
	mCameraNode->lookAt(Vector3(0, 0, CAMERA_LOOK_AT), Node::TS_PARENT);
	mCameraNode->roll(Degree(180));

	loadAnimations(sceneManager);
	loadSounds();
}

DFACore::DFALaserGuidedMissile::~DFALaserGuidedMissile()
{
	if(mObjectNode)
	{
		mObjectNode->detachObject(mNamePrefix + mName);
		mSceneManager->destroyEntity(mNamePrefix + mName);
		mSceneManager->destroySceneNode(mObjectNode);
	}
	if(mCameraNode)
		mSceneManager->destroySceneNode(mCameraNode);
	if(mMovementNode)
		mSceneManager->destroySceneNode(mMovementNode);
	
	if(mCollisionTool)
		delete mCollisionTool;

	if(mActor)
		DFAData::DFAPhysicsManager::getSingletonPtr()->getScene()->destroyActor(mActor);
}

void DFACore::DFALaserGuidedMissile::loadAnimations(Ogre::SceneManager *sceneManager)
{
	
}

void DFACore::DFALaserGuidedMissile::loadSounds()
{
	
}

void DFACore::DFALaserGuidedMissile::updateAnimations(const Ogre::Real frameRate)
{
	
}

void DFACore::DFALaserGuidedMissile::fire(const Ogre::Vector3 startPosition, const Ogre::Vector3 targetPosition, const Ogre::Vector3 ownerDirection)
{
	mStartPosition = startPosition;
	mTargetPosition = targetPosition;
	mDirection = ownerDirection;
	mDirection.normalise();
	Vector3 targetDirection = targetPosition - startPosition;
	targetDirection.normalise();
	mAngleToTarget = mDirection.angleBetween(targetDirection).valueDegrees();

	//set new orientation for actor
	Ogre::Quaternion nxOrientation = mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>();
	nxOrientation.normalise();
	Vector3 nxCurrDirection = nxOrientation * Vector3::NEGATIVE_UNIT_Y;
	Quaternion nxNewOrientation = nxCurrDirection.getRotationTo(mDirection);
	mActor->setGlobalOrientationQuat(nxNewOrientation);

	//set new orientation for Mesh
	/*Ogre::Quaternion orientation = mObjectNode->getOrientation();
	orientation.normalise();
	Vector3 currDirection = orientation * Vector3::NEGATIVE_UNIT_Y;
	Quaternion newOrientation = currDirection.getRotationTo(mDirection);
	mMovementNode->setOrientation(newOrientation);
	*/

	Vector3 direction = mObjectNode->getOrientation() * Vector3::NEGATIVE_UNIT_Y;
	direction.normalise();
	Ogre::Quaternion rotationQuat = direction.getRotationTo(mDirection);

	//positive number goes down
	Vector3 forward = mObjectNode->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Y;
	forward.normalise();
	//positive number goes clockwise
	Vector3 right = mObjectNode->getOrientation() * Ogre::Vector3::UNIT_X;
	right.normalise();

	Radian roll = rotationQuat.getRoll();
	Radian pitch = rotationQuat.getPitch();
	Radian yaw2 = rotationQuat.getYaw();

	
	Ogre::Quaternion rollPitch = Quaternion(rotationQuat.getRoll(), forward) *
		Quaternion(rotationQuat.getPitch(), right);
	Ogre::Quaternion yaw = Quaternion(rotationQuat.getPitch(), Vector3::NEGATIVE_UNIT_X);
	
	/*
	Ogre::Quaternion rollPitch = Quaternion(Degree(0), forward) *
		Quaternion(Degree(60), right);
	Ogre::Quaternion yaw = Quaternion(Degree(30), Vector3::UNIT_Z);
	*/

	//mMovementNode->setOrientation(mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>());
	mObjectNode->setOrientation(rollPitch);
	mMovementNode->setOrientation(yaw);


	//just testing
	//mPitchAngle = newOrientation.getPitch().valueDegrees();
	//mYawAngle = newOrientation.getYaw().valueDegrees();
	//mRollAngle = newOrientation.getRoll().valueDegrees();

	//set starting position
	setPosition(startPosition);
}

Vector3 DFACore::DFALaserGuidedMissile::getPosition(void)
{
	return mActor->getGlobalPosition().as<Ogre::Vector3>();
}

Ogre::String DFACore::DFALaserGuidedMissile::getNodeName(void)
{
	return mObjectNode->getName();
}

Ogre::SceneNode* DFACore::DFALaserGuidedMissile::getCameraNode(void)
{
	return mCameraNode;
}

void DFACore::DFALaserGuidedMissile::setPosition(const Ogre::Vector3 position)
{
	mActor->setGlobalPosition(position);
	Ogre::Quaternion orientation = mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>();
	orientation.normalise();
	Vector3 actorPosition = mActor->getGlobalPosition().as<Ogre::Vector3>();
	actorPosition += orientation * Vector3(ACTOR_POS_OFFSET_X, ACTOR_POS_OFFSET_Y, ACTOR_POS_OFFSET_Z);
	mMovementNode->setPosition(actorPosition);
}

void DFACore::DFALaserGuidedMissile::update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager)
{
	updatePosition(frameRate);
	updateAnimations(frameRate);
}

void DFACore::DFALaserGuidedMissile::updatePosition(const Ogre::Real frameRate)
{
	/*
	const Vector3 position = getPosition();
	const Vector3 targetDirection = (mTargetPosition - getPosition()).normalisedCopy();

	//set the orientation
	Vector3 direction = mObjectNode->getOrientation() * Vector3::NEGATIVE_UNIT_Y;
	direction.normalise();
	Ogre::Quaternion rotationQuat = direction.getRotationTo(targetDirection);
	
	Radian r;
	Vector3 axis;
	rotationQuat.ToAngleAxis(r, axis);
	Real deg = r.valueDegrees();
	/*if(r.valueDegrees() >= 10)
	{
		r = Degree(10);
		rotationQuat.FromAngleAxis(r, axis);
	}*

	//positive number goes down
	Vector3 forward = mObjectNode->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Y;
	forward.normalise();
	//positive number goes clockwise
	Vector3 right = mObjectNode->getOrientation() * Ogre::Vector3::UNIT_X;
	right.normalise();

	Radian roll = rotationQuat.getRoll();
	Radian pitch = rotationQuat.getPitch();
	Radian yaw2 = rotationQuat.getYaw();

	/*
	Ogre::Quaternion rollPitch = Quaternion(rotationQuat.getPitch(), forward) *
		Quaternion(rotationQuat.getRoll(), right);
	Ogre::Quaternion yaw = Quaternion(rotationQuat.getYaw(), Vector3::UNIT_Z);
	*
	Ogre::Quaternion rollPitch = Quaternion(Degree(0), forward) *
		Quaternion(Degree(60), right);
	Ogre::Quaternion yaw = Quaternion(Degree(30), Vector3::UNIT_Z);


	mActor->setGlobalOrientationQuat(rotationQuat);
	//mMovementNode->setOrientation(mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>());
	mObjectNode->setOrientation(rollPitch);
	mMovementNode->setOrientation(yaw);

*/
	/*
	//set new orientation for actor
	Ogre::Quaternion nxOrientation = mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>();
	nxOrientation.normalise();
	Vector3 nxCurrDirection = (nxOrientation * Vector3::NEGATIVE_UNIT_Y).normalisedCopy();
	Quaternion nxNewOrientation = nxCurrDirection.getRotationTo(targetDirection);
	Radian r;
	Vector3 axis;
	nxNewOrientation.ToAngleAxis(r, axis);
	//if(r.valueDegrees() >= 90)
		mActor->setGlobalOrientationQuat(nxNewOrientation);

	//set new orientation for Mesh
	Ogre::Quaternion orientation = mObjectNode->getOrientation();
	orientation.normalise();
	Vector3 currDirection = orientation * Vector3::NEGATIVE_UNIT_Y;
	Quaternion newOrientation = currDirection.getRotationTo(targetDirection);
	newOrientation.ToAngleAxis(r, axis);
	r = Degree(MAX_TURN_ANGLE);
	newOrientation.FromAngleAxis(r, axis);
	mMovementNode->setOrientation(newOrientation);
*/

	//calculate position
	/*const Real distance = mSpeed * frameRate;
	const Vector3 movePos = distance * mDirection;
	mDistanceTraveled += distance;

	//set position
	mActor->setLinearVelocity(movePos);
	orientation = mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>();
	orientation.normalise();
	Vector3 actorPosition = mActor->getGlobalPosition().as<Ogre::Vector3>();
	actorPosition += orientation * Vector3(ACTOR_POS_OFFSET_X, ACTOR_POS_OFFSET_Y, ACTOR_POS_OFFSET_Z);
	mMovementNode->setPosition(actorPosition);*/

	/*
	const Vector3 currentPosition = getPosition();
	Vector3 targetDirection = mTargetPosition - currentPosition;
	targetDirection.normalise();

	//set orientation for actor
	Ogre::Quaternion nxOrientation = mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>();
	nxOrientation.normalise();
	Vector3 nxCurrDirection = nxOrientation * Vector3::NEGATIVE_UNIT_Y;
	Quaternion nxNewOrientation = nxCurrDirection.getRotationTo(targetDirection);
	mActor->setGlobalOrientationQuat(nxNewOrientation);

	//set new orientation for Mesh
	Ogre::Quaternion orientation = mObjectNode->getOrientation();
	orientation.normalise();
	Vector3 currDirection = orientation * Vector3::NEGATIVE_UNIT_Y;
	Quaternion newOrientation = currDirection.getRotationTo(targetDirection);
	mMovementNode->setOrientation(newOrientation);

	orientation = mObjectNode->getOrientation();
	orientation.normalise();
	currDirection = orientation * Vector3::NEGATIVE_UNIT_Y;

	const Real distance = mSpeed * frameRate;
	const Vector3 movePos = distance * currDirection;
	mDistanceTraveled += distance;

	*/

	/*
	const Vector3 currentPosition = getPosition();
	Vector3 targetDirection = mTargetPosition - currentPosition;
	targetDirection.normalise();
	mDirection = mObjectNode->getOrientation() * Vector3::NEGATIVE_UNIT_Y;
	mDirection.normalise();
	//mAngleToTarget = mDirection.angleBetween(targetDirection).valueDegrees();

	//get pitch and yaw angles
	Ogre::Quaternion rotationQuat = mDirection.getRotationTo(targetDirection);

	//Vector3 tankDirection = mObjectNode->getOrientation() * Vector3(0,0,-1);
	//tankDirection.normalise(); tankDirection.y = 0;
	//targetDirection.y = 0;
	//Ogre::Quaternion rotationQuat = tankDirection.getRotationTo(targetDirection);
	mActor->setGlobalOrientationQuat(rotationQuat);
	mMovementNode->setOrientation(mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>());

	//move to position
	mDirection = mObjectNode->getOrientation() * Vector3::NEGATIVE_UNIT_Y;
	mDirection.normalise();
	const Real distance = mSpeed * frameRate;
	const Vector3 movePos = distance * mDirection;//(mDirection + Vector3::UNIT_Y);
	mDistanceTraveled += distance;
	//const Vector3 movePosition = (targetDirection + Vector3::NEGATIVE_UNIT_Y) * MAX_SPEED * frameRate;
	mActor->setLinearVelocity(movePos);
	mMovementNode->setPosition(mActor->getGlobalPosition().as<Ogre::Vector3>());
*/
	
	
	
	
	
	
	
	
	//get pitch angle
	//Ogre::Real test = rotationQuat.getRoll().valueDegrees();
	//Ogre::Real pitchAngle = rotationQuat.getPitch().valueDegrees();
	//Ogre::Real test2 = rotationQuat.getYaw().valueDegrees();
	
	/*Ogre::Radian rotationAngle;
	Ogre::Vector3 rotationAxis;
	rotationQuat.ToAngleAxis(rotationAngle, rotationAxis);
	if(Ogre::Radian(rotationAngle).valueDegrees() >= MAX_TURN_ANGLE)
		rotationAngle = Ogre::Degree(MAX_TURN_ANGLE);
	rotationQuat.FromAngleAxis(rotationAngle, rotationAxis);
*/
	/*
	if(Math::Abs(pitchAngle) >= MAX_TURN_ANGLE)
	{
		if(pitchAngle <= 0)
			mPitchAngle -= MAX_TURN_ANGLE;
		else
			mPitchAngle += MAX_TURN_ANGLE;
	}
	else
	{
		if(pitchAngle <= 0)
			mPitchAngle -= pitchAngle;
		else
			mPitchAngle += pitchAngle;
	}

	//get yaw angle
	Ogre::Real yawAngle = rotationQuat.getYaw().valueDegrees();
	if(Math::Abs(yawAngle) >= MAX_TURN_ANGLE)
	{
		if(yawAngle >= 0)
			mYawAngle += MAX_TURN_ANGLE;
		else
			mYawAngle -= MAX_TURN_ANGLE;
	}
	else
	{
		if(yawAngle >= 0)
			mYawAngle += yawAngle;
		else
			mYawAngle -= yawAngle;
	}
	*/
	
	//rotate the maximum angle for actor

	//rotate the maximum angle for mesh
	/*Ogre::Quaternion orientation = mObjectNode->getOrientation();
	orientation.normalise();
	Vector3 right = orientation * Ogre::Vector3(1.0, 0, 0);
	right.normalise();
	
	Ogre::Quaternion pitch = Ogre::Quaternion(rotationQuat.getPitch(), right);
	Ogre::Quaternion yaw = Quaternion(rotationQuat.getYaw(), Vector3::UNIT_Y);
	orientation = pitch * yaw;

	//set orientation
	mObjectNode->setOrientation(pitch);
	mMovementNode->setOrientation(yaw);*/

	
	//mDirection = orientation * Vector3::NEGATIVE_UNIT_Y;
	//mDirection.normalise();
	//mObjectNode->setOrientation(rotationQuat.getPitch());
	//mMovementNode->setOrientation(rotationQuat.getYaw());

	//mObjectNode->setOrientation(rotationQuat);

	/*

	mDirection = rotationQuat * Vector3::NEGATIVE_UNIT_Y;
	mDirection.normalise();

	const Real distance = mSpeed * frameRate;
	const Vector3 movePos = distance * mDirection;
	mDistanceTraveled += distance;

	//mAngleToTarget = mDirection.angleBetween(targetDirection).valueDegrees();
	
	//set position
	mActor->setLinearVelocity(movePos);
	Ogre::Quaternion orientation = mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>();
	orientation.normalise();
	Vector3 actorPosition = mActor->getGlobalPosition().as<Ogre::Vector3>();
	actorPosition += orientation * Vector3(ACTOR_POS_OFFSET_X, ACTOR_POS_OFFSET_Y, ACTOR_POS_OFFSET_Z);
	mMovementNode->setPosition(actorPosition);*/
}

Ogre::String DFACore::DFALaserGuidedMissile::getName(void)
{
	return mName;
}

Ogre::uint32 DFACore::DFALaserGuidedMissile::getQueryMask(void)
{
	return mQueryMask;
}

NxOgre::RigidBody* DFACore::DFALaserGuidedMissile::getRigidBody(void)
{
	return mActor;
}

bool DFACore::DFALaserGuidedMissile::isDestroyed(void)
{
	return mDestroyed;
}

void DFACore::DFALaserGuidedMissile::setTargetFlags(const Ogre::uint32 targetFlags)
{
	mTargetFlags = targetFlags;
}

void DFACore::DFALaserGuidedMissile::onContact(const NxOgre::ContactPair &pair)
{
	std::list<DFACore::DFAWorldObject*> worldObjects = DFAData::DFAWorldData::getSingletonPtr()->getWorldObjects();
	std::list<DFACore::DFAWorldObject*>::iterator iter = worldObjects.begin();
	for(;iter != worldObjects.end(); iter++)
	{
		if(pair.mSecond == (*iter)->getRigidBody())
		{
			DFACore::DFASceneObject *sceneObject = dynamic_cast<DFACore::DFASceneObject*>((*iter));
			if(sceneObject)
			{
				const Ogre::uint32 queryMask = sceneObject->getQueryMask();
				if((mTargetFlags & queryMask) == queryMask)
				{
					sceneObject->decreaseHealth(DAMAGE);
					mActor->setContactCallback(NULL);
					mDestroyed = true;
				}
			}
			break;
		}
		else if(pair.mSecond->isSceneGeometryBased())
		{
			mActor->setContactCallback(NULL);
			mDestroyed = true;
			break;
		}
	}
}