#include "DFAPhysicsManager.h"

using namespace Ogre;

DFAData::DFAPhysicsManager *DFAData::DFAPhysicsManager::mPhysicsManager = 0;

DFAData::DFAPhysicsManager::DFAPhysicsManager()
{
	//mPhysicsTimeController = 0;
	mVisualDebugger = 0;
}

DFAData::DFAPhysicsManager::~DFAPhysicsManager()
{
	if(mPhysicsWorld)
		mPhysicsWorld->destroyWorld(true);
}

void DFAData::DFAPhysicsManager::update(const Real frameRate)
{
	if(mPhysicsWorld)
		mPhysicsWorld->advance(frameRate);

	//updateVisualDebugger();
}

void DFAData::DFAPhysicsManager::updateVisualDebugger(void)
{
	if(mVisualDebugger)
	{
		mVisualDebugger->draw();
		mVisualDebuggerNode->needUpdate();
	}
}

void DFAData::DFAPhysicsManager::createWorld(Ogre::SceneManager *sceneManager, const bool enableDebugger)
{
	//create world
	mPhysicsWorld = NxOgre::World::createWorld();
	NxOgre::ResourceSystem::getSingleton()->openProtocol(new Critter::OgreResourceProtocol());

	//create scene
	NxOgre::SceneDescription sceneDescription;
	sceneDescription.mGravity = NxOgre::Constants::MEAN_EARTH_GRAVITY;
	sceneDescription.mUseHardware = false;
	mPhysicsScene = mPhysicsWorld->createScene(sceneDescription);

	//physical scene values
	mPhysicsScene->getMaterial(0)->setStaticFriction(0.5);
	mPhysicsScene->getMaterial(0)->setDynamicFriction(0.5);
	mPhysicsScene->getMaterial(0)->setRestitution(0.1);

	//create time controller
	//mPhysicsTimeController = NxOgre::TimeController::getSingleton();
	
	//create render system
	mRenderSystem = new Critter::RenderSystem(mPhysicsScene, sceneManager);

	//create debugger
	mEnableDebugger = enableDebugger;
	
	/*
	mVisualDebugger = mPhysicsWorld->getVisualDebugger();
	mVisualDebuggerRenderable = new Renderable(NxOgre::Enums::RenderableType_VisualDebugger);
	mVisualDebugger->setRenderable(mVisualDebuggerRenderable);
	mVisualDebuggerNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	mVisualDebuggerNode->attachObject(mVisualDebuggerRenderable);
	*/
	
	mVisualDebuggerRenderable = mRenderSystem->createRenderable(NxOgre::Enums::RenderableType::RenderableType_VisualDebugger);
	mVisualDebugger = new NxOgre::VisualDebugger(mPhysicsWorld);
	mVisualDebugger->setRenderable(mVisualDebuggerRenderable);
	mVisualDebuggerNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	mVisualDebuggerNode->attachObject(mVisualDebuggerRenderable);

	if(enableDebugger)
		mVisualDebugger->setVisualisationMode(NxOgre::Enums::VisualDebugger_ShowAll);
	else
		mVisualDebugger->setVisualisationMode(NxOgre::Enums::VisualDebugger_ShowNone);

	//setting remote debugger
	//NxOgre::RemoteDebugger *remoteDebugger = mPhysicsWorld->getRemoteDebugger();
	//remoteDebugger->connect();
}

NxOgre::World* DFAData::DFAPhysicsManager::getWorld(void)
{
	return mPhysicsWorld;
}

NxOgre::Scene* DFAData::DFAPhysicsManager::getScene(void)
{
	return mPhysicsScene;
}

void DFAData::DFAPhysicsManager::visualDebugger(const bool activate)
{
	mEnableDebugger = activate;
	if(activate)
		mVisualDebugger->setVisualisationMode(NxOgre::Enums::VisualDebugger_ShowAll);
	else
		mVisualDebugger->setVisualisationMode(NxOgre::Enums::VisualDebugger_ShowNone);
}

bool DFAData::DFAPhysicsManager::isVisualDebuggerActive(void)
{
	return mEnableDebugger;
}

DFAData::DFAPhysicsManager* DFAData::DFAPhysicsManager::getSingletonPtr(void)
{
	if(!mPhysicsManager)
	{
		mPhysicsManager = new DFAData::DFAPhysicsManager();
	}

	return mPhysicsManager;
}