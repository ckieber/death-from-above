#include "DFAPhysicsManager.h"
#include "DFAUnguidedMissile.h"
#include "DFAWorldData.h"
#include "OgreOggSound.h"

#define CAMERA_DIST 5
#define CAMERA_HEIGHT 2
#define CAMERA_LOOK_AT 1

#define MAX_RANGE 2000
#define MAX_SPEED 60000
#define DAMAGE 5000
#define MAX_DAMAGE_RADIUS 15

#define ACTOR_POS_OFFSET_X 0
#define ACTOR_POS_OFFSET_Y 0.8
#define ACTOR_POS_OFFSET_Z 0

using namespace Ogre;

DFACore::DFAUnguidedMissile::DFAUnguidedMissile(const int id, const String &name, Ogre::SceneManager *sceneManager, const Ogre::uint32 queryMask) : DFACore::DFAMissile(queryMask)
{
	mCollisionTool = new DFATools::DFACollisionTool(sceneManager);
	mDestroyed = false;
	mPlayDestroyedAnimation = false;
	mId = id;
	mName = name;
	mSceneManager = sceneManager;
	mTargetFlags = 0xFFFFFFFF;
	
	mSpeed = MAX_SPEED;
	mDamage = DAMAGE;
	mDirection = Vector3::ZERO;
	mDistanceTraveled = 0;

	//create missile object
	mNamePrefix = StringConverter::toString(id) + "_";
	mMovementNode = sceneManager->getRootSceneNode()->createChildSceneNode(mNamePrefix + "UnguidedMissileMovementNode");
	mObjectNode = mMovementNode->createChildSceneNode(mNamePrefix + "UnguidedMissileNode");
	mCameraNode = mMovementNode->createChildSceneNode(mNamePrefix + "UnguidedMissileCameraNode");
	Entity *missile = sceneManager->createEntity(mNamePrefix + mName, "S-8Rocket.mesh");
	missile->setQueryFlags(queryMask);
	mObjectNode->attachObject(missile);
	mObjectNode->scale(3.5, 3.5, 3.5);

	//create physics
	NxOgre::RigidBodyDescription desc;
	desc.mName = "S-8Rocket";
	desc.mMass = 12;
	desc.mDynamicRigidbodyFlags |= NxOgre::DynamicRigidbodyFlags::DisableGravity;
	desc.mDynamicRigidbodyFlags |= NxOgre::DynamicRigidbodyFlags::FreezeRotation;
	desc.mContactReportFlags |= NxOgre::Enums::ContactPairFlags_All;
	NxOgre::BoxDescription boxDesc;
	boxDesc.mSize = Vector3(0.08, 1.40, 0.08);
	mActor = DFAData::DFAPhysicsManager::getSingletonPtr()->getScene()->createActor(boxDesc, NxOgre::Matrix44::IDENTITY, desc);
	mActor->setContactCallback(this);

	//set camera node position
	mCameraNode->setPosition(Vector3(0, CAMERA_HEIGHT, -CAMERA_DIST));
	mCameraNode->lookAt(Vector3(0, 0, CAMERA_LOOK_AT), Node::TS_PARENT);
	mCameraNode->roll(Degree(180));

	//create particles
	ParticleSystem *ps = sceneManager->createParticleSystem(mNamePrefix + "RocketSmoke", "RocketSmoke_mod");
	mMovementNode->createChildSceneNode(mNamePrefix + "UnguidedMissileParticleNode");
	mMovementNode->attachObject(ps);

	loadAnimations(sceneManager);
	loadSounds();
}

DFACore::DFAUnguidedMissile::~DFAUnguidedMissile()
{
	if(mObjectNode)
	{
		mObjectNode->detachObject(mNamePrefix + mName);
		mSceneManager->destroyEntity(mNamePrefix + mName);
		mSceneManager->destroySceneNode(mObjectNode);
	}
	if(mCameraNode)
		mSceneManager->destroySceneNode(mCameraNode);
	if(mMovementNode)
		mSceneManager->destroySceneNode(mMovementNode);
	
	if(mCollisionTool)
		delete mCollisionTool;

	if(mActor)
		DFAData::DFAPhysicsManager::getSingletonPtr()->getScene()->destroyActor(mActor);
}

void DFACore::DFAUnguidedMissile::loadAnimations(Ogre::SceneManager *sceneManager)
{
	
}

void DFACore::DFAUnguidedMissile::loadSounds()
{
	
}

void DFACore::DFAUnguidedMissile::updateAnimations(const Ogre::Real frameRate)
{
	
}

void DFACore::DFAUnguidedMissile::fire(const Ogre::Vector3 startPosition, const Ogre::Vector3 targetPosition, const Ogre::Vector3 ownerDirection)
{
	mStartPosition = startPosition;
	mTargetPosition = targetPosition;
	mDirection = targetPosition - startPosition;
	mDirection.normalise();

	//set new orientation for actor
	Ogre::Quaternion nxOrientation = mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>();
	nxOrientation.normalise();
	Vector3 nxCurrDirection = nxOrientation * Vector3::NEGATIVE_UNIT_Y;
	Quaternion nxNewOrientation = nxCurrDirection.getRotationTo(mDirection);
	mActor->setGlobalOrientationQuat(nxNewOrientation);

	//set new orientation for Mesh
	Ogre::Quaternion orientation = mObjectNode->getOrientation();
	orientation.normalise();
	Vector3 currDirection = orientation * Vector3::NEGATIVE_UNIT_Y;
	Quaternion newOrientation = currDirection.getRotationTo(mDirection);
	mMovementNode->setOrientation(newOrientation);

	//set starting position
	setPosition(startPosition);
}

Vector3 DFACore::DFAUnguidedMissile::getPosition(void)
{
	return mActor->getGlobalPosition().as<Ogre::Vector3>();
}

Ogre::String DFACore::DFAUnguidedMissile::getNodeName(void)
{
	return mObjectNode->getName();
}

Ogre::SceneNode* DFACore::DFAUnguidedMissile::getCameraNode(void)
{
	return mCameraNode;
}

void DFACore::DFAUnguidedMissile::setPosition(const Ogre::Vector3 position)
{
	mActor->setGlobalPosition(position);
	Ogre::Quaternion orientation = mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>();
	orientation.normalise();
	Vector3 actorPosition = mActor->getGlobalPosition().as<Ogre::Vector3>();
	actorPosition += orientation * Vector3(ACTOR_POS_OFFSET_X, ACTOR_POS_OFFSET_Y, ACTOR_POS_OFFSET_Z);
	mMovementNode->setPosition(actorPosition);
}

void DFACore::DFAUnguidedMissile::update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager)
{
	updatePosition(frameRate);
	updateAnimations(frameRate);
}

void DFACore::DFAUnguidedMissile::updatePosition(const Ogre::Real frameRate)
{
	//get old and new position
	const Vector3 oldPosition = getPosition();
	const Real distance = mSpeed * frameRate;
	const Vector3 movePos = distance * mDirection;
	mDistanceTraveled += distance;

	/*
	if(mDistanceTraveled > MAX_RANGE)
	{
		mDestroyed = true;
		return;
	}
	*/

	//set position
	mActor->setLinearVelocity(movePos);
	Ogre::Quaternion orientation = mActor->getGlobalOrientationQuat().as<Ogre::Quaternion>();
	orientation.normalise();
	Vector3 actorPosition = mActor->getGlobalPosition().as<Ogre::Vector3>();
	actorPosition += orientation * Vector3(ACTOR_POS_OFFSET_X, ACTOR_POS_OFFSET_Y, ACTOR_POS_OFFSET_Z);
	mMovementNode->setPosition(actorPosition);
}

Ogre::String DFACore::DFAUnguidedMissile::getName(void)
{
	return mName;
}

Ogre::uint32 DFACore::DFAUnguidedMissile::getQueryMask(void)
{
	return mQueryMask;
}

NxOgre::RigidBody* DFACore::DFAUnguidedMissile::getRigidBody(void)
{
	return mActor;
}

bool DFACore::DFAUnguidedMissile::isDestroyed(void)
{
	return mDestroyed;
}

void DFACore::DFAUnguidedMissile::setTargetFlags(const Ogre::uint32 targetFlags)
{
	mTargetFlags = targetFlags;
}

void DFACore::DFAUnguidedMissile::onContact(const NxOgre::ContactPair &pair)
{
	//check if missile hit ground or stationary object
	if(pair.mSecond->isSceneGeometryBased())
	{
		//check if something is within the damage capsule
		std::list<DFACore::DFAWorldObject*> worldObjects = DFAData::DFAWorldData::getSingletonPtr()->getWorldObjects();
		std::list<DFACore::DFAWorldObject*>::iterator iter = worldObjects.begin();
		for(;iter != worldObjects.end(); iter++)
		{
			DFACore::DFASceneObject *sceneObject = dynamic_cast<DFACore::DFASceneObject*>((*iter));
			if(sceneObject)
			{
				const Ogre::uint32 queryMask = sceneObject->getQueryMask();
				if((mTargetFlags & queryMask) == queryMask)
				{
					const Real maxDamageRadius = pow(MAX_DAMAGE_RADIUS, 2.0);
					const Vector3 objectPosition = sceneObject->getPosition();
					const Real distance = getPosition().squaredDistance(objectPosition);
					if(distance < maxDamageRadius)
					{
						//object is within damage capsule
						//calculate the percentage of the distance where:
						//missile impact (100%) -> MAX_DAMAGE_RADIUS (0%)
						const Real damagePercent = (100.0 - (100.0 / maxDamageRadius * distance)) / 100.0;
						sceneObject->decreaseHealth(damagePercent * DAMAGE);
					}
				}
			}
		}

		mActor->setContactCallback(NULL);
		mDestroyed = true;
	}
	else
	{
		std::list<DFACore::DFAWorldObject*> worldObjects = DFAData::DFAWorldData::getSingletonPtr()->getWorldObjects();
		std::list<DFACore::DFAWorldObject*>::iterator iter = worldObjects.begin();
		for(;iter != worldObjects.end(); iter++)
		{
			if(pair.mSecond == (*iter)->getRigidBody())
			{
				DFACore::DFASceneObject *sceneObject = dynamic_cast<DFACore::DFASceneObject*>((*iter));
				if(sceneObject)
				{
					const Ogre::uint32 queryMask = sceneObject->getQueryMask();
					if((mTargetFlags & queryMask) == queryMask)
					{
						sceneObject->decreaseHealth(DAMAGE);
						mActor->setContactCallback(NULL);
						mDestroyed = true;
					}
				}
				break;
			}
		}
	}
}