#include <Ogre.h>

#include "DFAGameStateManager.h"
#include "DFAIntroState.h"
#include "DFAMenuState.h" //for debugging

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"

INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
int main(int argc, char **argv)
#endif
{
	DFACore::DFAGameStateManager *deathFromAboveGame = new DFACore::DFAGameStateManager();

	try
	{
		// initialize the game and switch to the first state
		deathFromAboveGame->start(DFACore::DFAMenuState::getInstance());
	}
	catch (Ogre::Exception& e)
	{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
		MessageBoxA(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
		std::cerr << "An exception has occured: " << e.getFullDescription();
#endif
	}

	//delete game;

	return 0;
}