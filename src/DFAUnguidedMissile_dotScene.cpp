#include "DFAUnguidedMissile.h"
#include "DFAWorldData.h"
#include "OgreOggSound.h"

#define CAMERA_DIST 5
#define CAMERA_HEIGHT 2
#define CAMERA_LOOK_AT 1

#define MAX_RANGE 2000
#define MAX_SPEED 500
#define DAMAGE 2500

using namespace Ogre;

DFACore::DFAUnguidedMissile::DFAUnguidedMissile()
{
	mHealth = 1;
	mDestroyed = false;
	mPlayDestroyedAnimation = false;
	
	mSpeed = MAX_SPEED;
	mDamage = DAMAGE;
	mDirection = Vector3::ZERO;
	mDistanceTraveled = 0;
}

DFACore::DFAUnguidedMissile::~DFAUnguidedMissile()
{
	if(mDotSceneNode)
		mDotSceneNode->Destroy();

	if(mCameraNode)
		delete mCameraNode;
	if(mObjectNode)
	{
		//Entity *mainRotor = sceneManager->getEntity(mNamePrefix + "polySurface124");
		Ogre::SceneNode* test2 = (Ogre::SceneNode*)mObjectNode->getChild(mNamePrefix + "polySurface124");
		Ogre::MovableObject* mov = test2->getAttachedObject(mNamePrefix + "polySurface124");
		delete mObjectNode;
	}
	if(mMovementNode)
		delete mMovementNode;

	if(mCollisionTool)
		delete mCollisionTool;
}

void DFACore::DFAUnguidedMissile::loadAnimations(Ogre::SceneManager *sceneManager)
{
	
}

void DFACore::DFAUnguidedMissile::loadSounds()
{
	
}

void DFACore::DFAUnguidedMissile::updateAnimations(const Ogre::Real frameRate)
{
	
}

void DFACore::DFAUnguidedMissile::createObject(const Ogre::String name, const int id, const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
								  OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager, const DFACore::QueryMask queryMask)
{
	mCollisionTool = new DFATools::DFACollisionTool(sceneManager);
	mId = id;
	mNamePrefix = StringConverter::toString(id) + "_";
	mName = name;
	mQueryMask = queryMask;
	mDotSceneNode = new OgreMax::OgreMaxScene();
	mDotSceneNode->SetNamePrefix(mNamePrefix);
	mDotSceneNode->Load(sceneName, renderWindow, loadOptions, sceneManager, sceneManager->getRootSceneNode()->createChildSceneNode(mNamePrefix + "UnguidedMissileMovementNode")->createChildSceneNode(mNamePrefix + "UnguidedMissileNode"));

	mMovementNode = sceneManager->getSceneNode(mNamePrefix + "UnguidedMissileMovementNode");
	mObjectNode = sceneManager->getSceneNode(mNamePrefix + "UnguidedMissileNode");
	mCameraNode = mMovementNode->createChildSceneNode(mNamePrefix + "UnguidedMissileCameraNode");
	
	//mMovementNode->setPosition(Vector3(500, 150, 500));

	//set camera node position
	mCameraNode->setPosition(Vector3(0, CAMERA_HEIGHT, -CAMERA_DIST));
	mCameraNode->lookAt(Vector3(0, 0, CAMERA_LOOK_AT), Node::TS_PARENT);
	mCameraNode->roll(Degree(180));

	//for better testing
	//mAvatarNode->yaw(Degree(-45));
	//mTankNode->scale(Vector3(0.08, 0.08, 0.08));

	//Entity *ent = sceneManager->createEntity("Knot", "knot.mesh");
	SceneNode *moveNode = mObjectNode->createChildSceneNode(mNamePrefix + "UnguidedMissileMoveNode");
	moveNode->setPosition(Vector3(0, 0, 100));
	//moveNode->attachObject(ent);
	//moveNode->setScale(0.01, 0.01, 0.01);

	loadAnimations(sceneManager);
	loadSounds();
}

void DFACore::DFAUnguidedMissile::fire(const Ogre::Vector3 startPosition, const Ogre::Vector3 targetPosition, const Ogre::Vector3 ownerDirection)
{
	mStartPosition = startPosition;
	mTargetPosition = targetPosition;
	mDirection = targetPosition - startPosition;
	mDirection.normalise();
	setPosition(startPosition);
	
	//get the direction
	Quaternion orientation = mObjectNode->getOrientation();
	orientation.normalise();
	Vector3 forward = orientation * Ogre::Vector3(0, 0, 1.0);
	forward.normalise();
	Vector3 right = orientation * Ogre::Vector3(-1.0, 0, 0);
	right.normalise();

	//get horizontal angle in between
	Vector3 forwardHorizontal = forward;
	forwardHorizontal.y = 0;
	Vector3 directionHorizontal = mDirection;
	directionHorizontal.y = 0;
	Radian angleYaw = forwardHorizontal.angleBetween(directionHorizontal);

	//get vertical angle in between
	Vector3 forwardVertical = forward;
	forwardVertical.x = 0;
	forwardVertical.z = 0;
	Vector3 directionVertical = mDirection;
	directionVertical.x = 0;
	directionVertical.z = 0;
	Radian anglePitch = forwardVertical.angleBetween(directionVertical);

	Ogre::Quaternion pitch = Quaternion(anglePitch, right);
	Ogre::Quaternion yaw = Quaternion(angleYaw, Vector3::UNIT_Y);

	mObjectNode->setOrientation(pitch);
	mMovementNode->setOrientation(yaw);
}

Vector3 DFACore::DFAUnguidedMissile::getPosition(void)
{
	return mMovementNode->getPosition();
}

Ogre::String DFACore::DFAUnguidedMissile::getNodeName(void)
{
	return mObjectNode->getName();
}

Ogre::SceneNode* DFACore::DFAUnguidedMissile::getCameraNode(void)
{
	return mCameraNode;
}

void DFACore::DFAUnguidedMissile::setPosition(const Ogre::Vector3 position)
{
	mMovementNode->setPosition(position);
}

void DFACore::DFAUnguidedMissile::update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager)
{
	updatePosition(frameRate);
	updateAnimations(frameRate);
}

void DFACore::DFAUnguidedMissile::updatePosition(const Ogre::Real frameRate)
{
	//get old and new position
	const Vector3 oldPosition = getPosition();
	const Real distance = mSpeed * frameRate;
	const Vector3 newPosition = oldPosition + (distance * mDirection);
	mDistanceTraveled += distance;

	
	bool destroy = false;
	Ogre::MovableObject *target;
	const uint32 queryFlags = (~AVATAR) & (ENEMY | STATIONARY);
	Ogre::String hitTarget = "";//mCollisionTool->getCollisionTargetType(oldPosition, newPosition, target, 0.04, 0, queryFlags);
	if(hitTarget == "SceneObject")
	{
		DFAData::DFAWorldData::getSingletonPtr()->getSceneObjectFromMesh(target)->decreaseHealth(mDamage);
		destroy = true;
	}
	else if(hitTarget == "WorldFragment")
		destroy = true;
	else if(mDistanceTraveled > MAX_RANGE)
		destroy = true;

	if(destroy)
	{
		mPlayDestroyedAnimation = true;
		mDestroyed = true;
	}
	else
	{
		setPosition(newPosition);
	}
}

OgreMax::OgreMaxScene::ObjectExtraDataMap& DFACore::DFAUnguidedMissile::getObjectExtraDataMap(void)
{
	return mDotSceneNode->GetAllObjectExtraData();
}

Ogre::String DFACore::DFAUnguidedMissile::getName(void)
{
	return mName;
}

DFACore::QueryMask DFACore::DFAUnguidedMissile::getQueryMask(void)
{
	return mQueryMask;
}

bool DFACore::DFAUnguidedMissile::isDestroyed(void)
{
	return mDestroyed;
}

void DFACore::DFAUnguidedMissile::decreaseHealth(const Ogre::Real damage)
{
	mHealth -= damage;
	if(mHealth <= 0)
		mPlayDestroyedAnimation = true;
}