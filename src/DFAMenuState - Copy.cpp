#include <Ogre.h>

#include "DFAMenuState.h"
#include "DFAPauseState.h"
#include "DFAPlayState.h"
#include "DFAPilotManager.h"

using namespace Ogre;

DFACore::DFAMenuState DFACore::DFAMenuState::mMenuState;

void DFACore::DFAMenuState::enter()
{
	mRoot = Root::getSingletonPtr();
	mKeyboard = DFAInput::DFAInputManager::getSingletonPtr()->getKeyboard();
	mMouse = DFAInput::DFAInputManager::getSingletonPtr()->getMouse();

	mSceneMgr = mRoot->createSceneManager(ST_GENERIC);
	mCamera = mSceneMgr->createCamera("MenuCamera");
	mViewport = mRoot->getAutoCreatedWindow()->addViewport(mCamera);

	initGUI();
	loadMainMenu();

	/*manually create the quit button
	CEGUI::WindowManager *win = CEGUI::WindowManager::getSingletonPtr();
	CEGUI::Window *sheet = win->createWindow("DefaultGUISheet", "CEGUIDemo/Sheet");

	CEGUI::Window *quit = win->createWindow("TaharezLook/Button", "CEGUIDemo/QuitButton");
	quit->setText("Quit");
	quit->setSize(CEGUI::UVector2(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.05, 0)));

	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::quit, this));

	sheet->addChildWindow(quit);
	mSystem->setGUISheet(sheet);
	*/

	mExitGame = false;
}

void DFACore::DFAMenuState::exit()
{
	mSceneMgr->clearScene();
	mSceneMgr->destroyAllCameras();
	mRoot->getAutoCreatedWindow()->removeAllViewports();
}

void DFACore::DFAMenuState::pause()
{
}

void DFACore::DFAMenuState::resume()
{
}

bool DFACore::DFAMenuState::keyClicked(const OIS::KeyEvent &e)
{
	return true;
}

bool DFACore::DFAMenuState::keyPressed(const OIS::KeyEvent &e)
{
	CEGUI::System *sys = CEGUI::System::getSingletonPtr();
	sys->injectKeyDown(e.key);
	sys->injectChar(e.text);
	return true;
}

bool DFACore::DFAMenuState::keyReleased(const OIS::KeyEvent &e)
{
	CEGUI::System::getSingleton().injectKeyUp(e.key);
	return true;
}

bool DFACore::DFAMenuState::mouseMoved(const OIS::MouseEvent &e)
{
	CEGUI::System::getSingleton().injectMouseMove(e.state.X.rel, e.state.Y.rel);
	return true;
}

bool DFACore::DFAMenuState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
	return true;
}

bool DFACore::DFAMenuState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
	return true;
}

bool DFACore::DFAMenuState::frameStarted(const FrameEvent &evt)
{
	DFAInput::DFAInputManager::getSingletonPtr()->capture();
	return !mExitGame && !mKeyboard->isKeyDown(OIS::KC_ESCAPE);
}

bool DFACore::DFAMenuState::frameEnded(const FrameEvent &evt)
{
	return !mExitGame;
}

CEGUI::MouseButton DFACore::DFAMenuState::convertButton(OIS::MouseButtonID buttonID)
{
	switch(buttonID)
	{
		case OIS::MB_Left:
			return CEGUI::LeftButton;
		case OIS::MB_Middle:
			return CEGUI::MiddleButton;
		case OIS::MB_Right:
			return CEGUI::RightButton;
		default:
			return CEGUI::LeftButton;
	}
}

void DFACore::DFAMenuState::initGUI()
{
	mRenderer = new CEGUI::OgreCEGUIRenderer(mRoot->getAutoCreatedWindow(), Ogre::RENDER_QUEUE_OVERLAY, false, 3000, mSceneMgr);
	mSystem = new CEGUI::System(mRenderer);

	CEGUI::SchemeManager::getSingleton().loadScheme((CEGUI::utf8*)"TaharezLookSkin.scheme");
	mSystem->setDefaultMouseCursor((CEGUI::utf8*)"TaharezLook", (CEGUI::utf8*)"MouseArrow");
	mSystem->setDefaultFont((CEGUI::utf8*)"BlueHighway-12");
	CEGUI::MouseCursor::getSingleton().setImage(CEGUI::System::getSingleton().getDefaultMouseCursor());

	CEGUI::ImagesetManager::getSingletonPtr()->createImagesetFromImageFile("BackgroundImage", "Background.tga");
	CEGUI::ImagesetManager::getSingletonPtr()->createImagesetFromImageFile("CreditsImage", "Credits.jpg");
}

bool DFACore::DFAMenuState::quitMainMenu(const CEGUI::EventArgs &e)
{
	mExitGame = true;
	return !mExitGame;
}

bool DFACore::DFAMenuState::startMissionMainMenu(const CEGUI::EventArgs &e)
{
	changeState(DFACore::DFAPlayState::getInstance());
	return true;
}

void DFACore::DFAMenuState::subscribeMainMenuButtons()
{
	CEGUI::Window *startMission = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/startMission");
	startMission->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::startMissionMainMenu, this));

	CEGUI::Window *pilotLog = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/pilotLog");
	pilotLog->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadPilotLogMenu, this));

	CEGUI::Window *settings = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/settings");
	settings->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadSettingsMenu, this));

	CEGUI::Window *credits = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/credits");
	credits->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadCreditsMenu, this));

	CEGUI::Window *quit = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/quit");
	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::quitMainMenu, this));
}

void DFACore::DFAMenuState::subscribePilotLogButtons()
{
	CEGUI::Window *startMission = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/startMission");
	startMission->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::startMissionMainMenu, this));

	CEGUI::Window *mainMenu = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/mainMenu");
	mainMenu->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadMainMenu, this));

	CEGUI::Window *settings = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/settings");
	settings->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadSettingsMenu, this));

	CEGUI::Window *credits = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/credits");
	credits->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadCreditsMenu, this));

	CEGUI::Window *quit = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/quit");
	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::quitMainMenu, this));

	CEGUI::Window *pilot = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/frameWindowPilots/");
	pilot->subscribeEvent(CEGUI::Listbox::EventSelectionChanged, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::handlePilotSelection, this));

}

bool DFACore::DFAMenuState::handlePilotSelection(const CEGUI::EventArgs &e)
{
	//get selected item
	const CEGUI::WindowEventArgs &windowEventArgs = static_cast<const CEGUI::WindowEventArgs&>(e);
	CEGUI::ListboxItem *item = static_cast<CEGUI::Listbox*>(windowEventArgs.window)->getFirstSelectedItem();

	if(item)
	{
		//add the selection colour
		item->setSelectionColours(CEGUI::colour(192, 192, 192, 1));
		item->setSelectionBrushImage("TaharezLook", "ListboxSelectionBrush");

		//if the item exists add it to statistic box
		DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
		CEGUI::DefaultWindow *textBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/statisticWindow/statistic");
		textBox->setText(pilotManager->printPilot(item->getText()));

		CEGUI::DefaultWindow *nameBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/nameWindow/nameBox");
		nameBox->setText(item->getText());
	}
	else
	{
		CEGUI::DefaultWindow *textBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/statisticWindow/statistic");
		textBox->setText("");

		CEGUI::DefaultWindow *nameBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/nameWindow/nameBox");
		nameBox->setText("");
	}

	return true;
}

void DFACore::DFAMenuState::subscribeSettingsButtons()
{
	CEGUI::Window *startMission = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/startMission");
	startMission->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::startMissionMainMenu, this));

	CEGUI::Window *pilotLog = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/pilotLog");
	pilotLog->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadPilotLogMenu, this));

	CEGUI::Window *mainMenu = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/mainMenu");
	mainMenu->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadMainMenu, this));

	CEGUI::Window *credits = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/credits");
	credits->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadCreditsMenu, this));

	CEGUI::Window *quit = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/quit");
	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::quitMainMenu, this));

	CEGUI::Window *keyAssign = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/keyWindow/keyList");
	keyAssign->subscribeEvent(CEGUI::Listbox::EventSelectionChanged, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::handleKeySelection, this));
}

bool DFACore::DFAMenuState::handleKeySelection(const CEGUI::EventArgs &e)
{
	//get selected item
	const CEGUI::WindowEventArgs &windowEventArgs = static_cast<const CEGUI::WindowEventArgs&>(e);
	CEGUI::ListboxItem *item = static_cast<CEGUI::Listbox*>(windowEventArgs.window)->getFirstSelectedItem();

	if(item)
	{
		//add the selection colour
		item->setSelectionColours(CEGUI::colour(192, 192, 192, 1));
		item->setSelectionBrushImage("TaharezLook", "ListboxSelectionBrush");

		//if the item exists add it to statistic box
		//DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
		//CEGUI::DefaultWindow *textBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/statisticWindow/statistic");
		//textBox->setText(pilotManager->printPilot(item->getText()));
	}

	return true;
}

void DFACore::DFAMenuState::subscribeCreditsButtons()
{
	CEGUI::Window *startMission = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/startMission");
	startMission->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::startMissionMainMenu, this));

	CEGUI::Window *pilotLog = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/pilotLog");
	pilotLog->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadPilotLogMenu, this));

	CEGUI::Window *settings = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/settings");
	settings->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadSettingsMenu, this));

	CEGUI::Window *mainMenu = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/mainMenu");
	mainMenu->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadMainMenu, this));

	CEGUI::Window *quit = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/quit");
	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::quitMainMenu, this));
}

void DFACore::DFAMenuState::loadMainMenu()
{
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
	CEGUI::Window *sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout((CEGUI::utf8*)"mainMenu.layout");
	
	//load image into the staticImage holder
	CEGUI::Window *background = CEGUI::WindowManager::getSingleton().getWindow("imageBackground");
	CEGUI::DefaultWindow *staticImage = static_cast<CEGUI::DefaultWindow*>(background);
	staticImage->setProperty("Image", "set:BackgroundImage image:full_image");

	subscribeMainMenuButtons();

	mSystem->setGUISheet(sheet);
}

bool DFACore::DFAMenuState::loadMainMenu(const CEGUI::EventArgs &e)
{
	loadMainMenu();
	return true;
}

bool DFACore::DFAMenuState::loadPilotLogMenu(const CEGUI::EventArgs &e)
{
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
	CEGUI::Window *sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout((CEGUI::utf8*)"pilotLog.layout");
	
	//load image into the staticImage holder
	CEGUI::Window *background = CEGUI::WindowManager::getSingleton().getWindow("imageBackground");
	CEGUI::DefaultWindow *staticImage = static_cast<CEGUI::DefaultWindow*>(background);
	staticImage->setProperty("Image", "set:BackgroundImage image:full_image");

	CEGUI::Listbox *list = (CEGUI::Listbox*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/frameWindowPilots/");

	DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
	pilotManager->loadDataFromFile();

	/*
	DFAData::DFAPilot *newPilot = pilotManager->createPilot("Christian Kieber");
	newPilot->addKills(11);
	newPilot->increaseCompMissionsNight();
	newPilot->increaseCompMissionsNight();
	newPilot->increaseCompMissionsDay();
	newPilot->increaseIncompMissionsNight();
	newPilot->setStatus(DFAData::DFAPilot::INJURED);

	newPilot = pilotManager->createPilot("Jeremy Weber");
	newPilot->promote();
	*/
	//testing
	//pilotManager->saveDataToFile();

	// add all the pilots in mPilots to the list
	std::list<DFAData::DFAPilot*> *pilotList = pilotManager->getPilotList();
	std::list<DFAData::DFAPilot*>::iterator iter = pilotList->begin();
	for(; iter != pilotList->end(); iter++)
	{
		CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem((*iter)->getName());
		list->addItem(item);
	}

	subscribePilotLogButtons();

	mSystem->setGUISheet(sheet);
	return true;
}

bool DFACore::DFAMenuState::loadSettingsMenu(const CEGUI::EventArgs &e)
{
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
	CEGUI::Window *sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout((CEGUI::utf8*)"settings.layout");
	
	//load image into the staticImage holder
	CEGUI::Window *background = CEGUI::WindowManager::getSingleton().getWindow("imageBackground");
	CEGUI::DefaultWindow *staticImage = static_cast<CEGUI::DefaultWindow*>(background);
	staticImage->setProperty("Image", "set:BackgroundImage image:full_image");

	//just for testing
	CEGUI::Listbox *list = (CEGUI::Listbox*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/keyWindow/keyList");

	CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem("Forwards ---> Up Arrow");
	list->addItem(item);
	item = new CEGUI::ListboxTextItem("Backwards ---> Down Arrow");
	list->addItem(item);
	item = new CEGUI::ListboxTextItem("Roll left ---> Left Arrow");
	list->addItem(item);
	item = new CEGUI::ListboxTextItem("Roll right ---> Right Arrow");
	list->addItem(item);
	item = new CEGUI::ListboxTextItem("Up ---> Page Up");
	list->addItem(item);
	item = new CEGUI::ListboxTextItem("Down ---> Page Down");
	list->addItem(item);
	//stop testing

	subscribeSettingsButtons();

	mSystem->setGUISheet(sheet);
	return true;
}

bool DFACore::DFAMenuState::loadCreditsMenu(const CEGUI::EventArgs &e)
{
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
	CEGUI::Window *sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout((CEGUI::utf8*)"credits.layout");
	
	//load image into the staticImage holder
	CEGUI::Window *background = CEGUI::WindowManager::getSingleton().getWindow("imageBackground");
	CEGUI::DefaultWindow *staticImage = static_cast<CEGUI::DefaultWindow*>(background);
	staticImage->setProperty("Image", "set:BackgroundImage image:full_image");

	CEGUI::Window *creditsImage = CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/picture");
	staticImage = static_cast<CEGUI::DefaultWindow*>(creditsImage);
	staticImage->setProperty("Image", "set:CreditsImage image:full_image");

	subscribeCreditsButtons();

	mSystem->setGUISheet(sheet);
	return true;
}