#include <Ogre.h>

#include "DFAIntroState.h"
#include "DFALogoState.h"

#define VIDTEXWIDTH 550
#define VIDTEXHEIGHT 400


using namespace Ogre;

DFACore::DFAIntroState DFACore::DFAIntroState::mIntroState;

void DFACore::DFAIntroState::enter()
{
	mRoot = Root::getSingletonPtr();
	mKeyboard = DFAInput::DFAInputManager::getSingletonPtr()->getKeyboard();
	mMouse = DFAInput::DFAInputManager::getSingletonPtr()->getMouse();

	mSceneMgr = mRoot->createSceneManager(ST_GENERIC);
	mCamera = mSceneMgr->createCamera("IntroCamera");
	mViewport = mRoot->getAutoCreatedWindow()->addViewport(mCamera);

	mCamera->setAspectRatio(Real(mViewport->getActualWidth())/Real(mViewport->getActualHeight()));
	mCamera->setPosition(Vector3(0,0,500));
	mCamera->lookAt(Vector3::ZERO);

	createScene();

	mExitGame = false;
}

void DFACore::DFAIntroState::exit()
{
	//stop using texture and destroy video object
	Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().load("VideoMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	Ogre::TextureUnitState *tex = mat->getTechnique(0)->getPass(0)->getTextureUnitState(0);
	tex->setTextureName(Ogre::String(""));

	delete mDshowMovieTextureSystem;

	mSceneMgr->clearScene();
	mSceneMgr->destroyAllCameras();
	mRoot->getAutoCreatedWindow()->removeAllViewports();
}

void DFACore::DFAIntroState::pause()
{
}

void DFACore::DFAIntroState::resume()
{
}

bool DFACore::DFAIntroState::keyClicked(const OIS::KeyEvent &e)
{
	return true;
}

bool DFACore::DFAIntroState::keyPressed(const OIS::KeyEvent &e)
{
	switch(e.key)
	{
		case OIS::KC_ESCAPE:
			changeState(DFACore::DFALogoState::getInstance());
			break;
		default:
			break;
	}
	return !mExitGame;
}

bool DFACore::DFAIntroState::keyReleased(const OIS::KeyEvent &e)
{
	return true;
}

bool DFACore::DFAIntroState::mouseMoved(const OIS::MouseEvent &e)
{
	return true;
}

bool DFACore::DFAIntroState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true;
}

bool DFACore::DFAIntroState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true;
}

bool DFACore::DFAIntroState::frameStarted(const FrameEvent &evt)
{
	mDshowMovieTextureSystem->updateMovieTexture();
	//DFAInput::DFAInputManager::getSingletonPtr()->capture();
	
	return true;
}

bool DFACore::DFAIntroState::frameEnded(const FrameEvent &evt)
{
	if(!mDshowMovieTextureSystem->isPlayingMovie())
		changeState(DFACore::DFALogoState::getInstance());
	return true;
}

void DFACore::DFAIntroState::captureInput(void)
{
	DFAInput::DFAInputManager::getSingletonPtr()->capture();
}

void DFACore::DFAIntroState::processLogic(const Real frameRate)
{

}

void DFACore::DFAIntroState::createScene()
{
	MeshPtr windowPlane = MeshManager::getSingleton().createPlane("Window",
           ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Plane(Vector3::UNIT_Z, 0), VIDTEXWIDTH , VIDTEXHEIGHT);

	Entity *ent = mSceneMgr->createEntity("WindowEntity", "Window");
	SceneNode *node = mSceneMgr->getRootSceneNode()->createChildSceneNode("WindowNode");
	ent->setMaterial(Ogre::MaterialManager::getSingleton().load("VideoMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME));
	node->attachObject(ent);
	//adjust position so it is in the center of the screen
	node->setPosition(VIDTEXWIDTH/1024, -VIDTEXHEIGHT/1024, 0);

	//just testing the video
	mDshowMovieTextureSystem = new DFAVideo::DirectShowMovieTexture(640, 480);
	Ogre::String moviePath = "../../Media/Videos/DFA Intro.avi";
	mDshowMovieTextureSystem->loadMovie(moviePath);
	mDshowMovieTextureSystem->playMovie();

	Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().load("VideoMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	Ogre::TextureUnitState *tex = mat->getTechnique(0)->getPass(0)->getTextureUnitState(0);
	tex->setTextureName(mDshowMovieTextureSystem->getMovieTexture()->getName());
}