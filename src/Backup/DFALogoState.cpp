#include <Ogre.h>

#include "DFALogoState.h"
#include "DFAMenuState.h"
#include "DFAFader.h"

using namespace Ogre;

DFACore::DFALogoState DFACore::DFALogoState::mLogoState;

void DFACore::DFALogoState::enter()
{
	mRoot = Root::getSingletonPtr();
	mKeyboard = DFAInput::DFAInputManager::getSingletonPtr()->getKeyboard();
	mMouse = DFAInput::DFAInputManager::getSingletonPtr()->getMouse();

	mSceneMgr = mRoot->createSceneManager(ST_GENERIC);
	mCamera = mSceneMgr->createCamera("IntroCamera");
	mViewport = mRoot->getAutoCreatedWindow()->addViewport(mCamera);

	mLogo = Ogre::OverlayManager::getSingletonPtr()->getByName("LogoOverlay");
	mLogo->show();

	mLogoFadeDone = false;
	mFader = new DFATools::DFAFader("FadeInOut", "FadeMaterial", this);
	mFader->startFadeIn(3.5);

	mExitGame = false;
}

void DFACore::DFALogoState::exit()
{
	mLogo->hide();

	mSceneMgr->clearScene();
	mSceneMgr->destroyAllCameras();
	mRoot->getAutoCreatedWindow()->removeAllViewports();
}

void DFACore::DFALogoState::pause()
{
}

void DFACore::DFALogoState::resume()
{
}

bool DFACore::DFALogoState::keyClicked(const OIS::KeyEvent &e)
{
	return true;
}

bool DFACore::DFALogoState::keyPressed(const OIS::KeyEvent &e)
{
	switch(e.key)
	{
		case OIS::KC_ESCAPE:
			if(mLogoFadeDone)
				changeState(DFACore::DFAMenuState::getInstance());
			break;
		default:
			break;
	}
	return !mExitGame;
}

bool DFACore::DFALogoState::keyReleased(const OIS::KeyEvent &e)
{
	return true;
}

bool DFACore::DFALogoState::mouseMoved(const OIS::MouseEvent &e)
{
	return true;
}

bool DFACore::DFALogoState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true;
}

bool DFACore::DFALogoState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true;
}

bool DFACore::DFALogoState::frameStarted(const FrameEvent &evt)
{
	//DFAInput::DFAInputManager::getSingletonPtr()->capture();
	//mFader->fade(evt.timeSinceLastFrame);
	return true;
}

bool DFACore::DFALogoState::frameEnded(const FrameEvent &evt)
{
	return true;
}

void DFACore::DFALogoState::captureInput(void)
{
	DFAInput::DFAInputManager::getSingletonPtr()->capture();
}

void DFACore::DFALogoState::processLogic(const Real frameRate)
{
	mFader->fade(frameRate);
}

void DFACore::DFALogoState::fadeInCallback()
{
	mLogoFadeDone = true;
}

void DFACore::DFALogoState::fadeOutCallback()
{
}