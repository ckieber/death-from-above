//#include <Ogre.h>
#include <Windows.h>

#include "DFAFrameManager.h"
#include "DFAGameStateManager.h"
#include "DFAInputManager.h"
#include "DFAGameState.h"
#include "DFAPhysicsManager.h"

#define LOGIC_FPS 60

using namespace Ogre;

template<> DFACore::DFAGameStateManager *Singleton<DFACore::DFAGameStateManager>::ms_Singleton = 0;

DFACore::DFAGameStateManager::DFAGameStateManager()
{
	mRoot = 0;
	mInputManager = 0;
	mKeyboard = 0;
	mMouse = 0;
}

DFACore::DFAGameStateManager::~DFAGameStateManager()
{
	// clean up all the states
	while (!mStates.empty()) {
		mStates.back()->exit();
		mStates.pop_back();
	}

	if(mInputManager)
		delete mInputManager;
	if(mRoot)
		delete mRoot;
}

void DFACore::DFAGameStateManager::start(DFACore::DFAGameState *state)
{
	mRoot = new Root();

	setupResources();

	if (!configure()) return;

	mInputManager = DFAInput::DFAInputManager::getSingletonPtr();
	mInputManager->initialise(mRoot->getAutoCreatedWindow());
	mInputManager->addKeyListener(this, "keyListener");
	mInputManager->addMouseListener(this, "mouseListener");

	mKeyboard = mInputManager->getKeyboard();
	mMouse = mInputManager->getMouse();

	mRoot->addFrameListener(this);

	changeState(state);

	//mRoot->startRendering();

	//set updateFrameFrequency -> mostly for raycasting, i.e raycast every 4th frame
	DFAData::DFAFrameManager::getSingletonPtr()->setFrameUpdateFrequency(4);

	//manual game loop
	bool exitGame = false;
	LARGE_INTEGER performanceFrequency, oldTime, time;
	Real logicFpsStep = 1.0 / LOGIC_FPS, frameDelta = 0, logicUpdateCounter = 0;
	QueryPerformanceFrequency(&performanceFrequency);
	QueryPerformanceCounter(&oldTime);

	while(!exitGame)
	{
		QueryPerformanceCounter(&time);
		WindowEventUtilities::messagePump();
		mStates.back()->captureInput();

		LARGE_INTEGER diff;
		diff.QuadPart = time.QuadPart - oldTime.QuadPart;
		frameDelta = (((Real)diff.QuadPart) / ((Real)performanceFrequency.QuadPart));
		logicUpdateCounter += frameDelta;
		while(logicUpdateCounter >= logicFpsStep)
		{
			logicUpdateCounter -= logicFpsStep;
			mStates.back()->processLogic(logicFpsStep);
		}

		DFAData::DFAPhysicsManager::getSingletonPtr()->updateVisualDebugger();
		exitGame = !mRoot->renderOneFrame();
		DFAData::DFAFrameManager::getSingletonPtr()->update();
		//if(DFAData::DFAFrameManager::getSingletonPtr()->isInUpdateFrame(4))
			//DFAData::DFAPhysicsManager::getSingletonPtr()->update(logicFpsStep);
		oldTime = time;
	}
}

void DFACore::DFAGameStateManager::changeState(DFACore::DFAGameState* state)
{
	// cleanup the current state
	if (!mStates.empty())
	{
		mStates.back()->exit();
		mStates.pop_back();
	}

	// store and init the new state
	mStates.push_back(state);
	mStates.back()->enter();
}

void DFACore::DFAGameStateManager::pushState(DFACore::DFAGameState* state)
{
	// pause current state
	if (!mStates.empty())
		mStates.back()->pause();

	// store and init the new state
	mStates.push_back(state);
	mStates.back()->enter();
}

void DFACore::DFAGameStateManager::popState()
{
	// cleanup the current state
	if (!mStates.empty())
	{
		mStates.back()->exit();
		mStates.pop_back();
	}

	// resume previous state
	if (!mStates.empty())
		mStates.back()->resume();
}

void DFACore::DFAGameStateManager::setupResources(void)
{
	String secName, typeName, archName;
	ConfigFile cf;
	cf.load("resources.cfg");

	ConfigFile::SectionIterator seci = cf.getSectionIterator();
	while(seci.hasMoreElements()){
		secName = seci.peekNextKey();
		ConfigFile::SettingsMultiMap *settings = seci.getNext();
		ConfigFile::SettingsMultiMap::iterator i;
		for(i = settings->begin(); i != settings->end(); i++){
			typeName = i->first;
			archName = i->second;
			ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
		}
	}
}

bool DFACore::DFAGameStateManager::configure(void)
{
	//for testing always show config box
	mRoot->restoreConfig();
	mRoot->showConfigDialog();
	/*
	// load config settings from ogre.cfg
	if (!mRoot->restoreConfig())
	{
		// if there is no config file, show the configuration dialog
		if (!mRoot->showConfigDialog())
		{
			return false;
		}
	}
	*/

	// initialise and create a default rendering window
	mRenderWindow = mRoot->initialise(true);

	TextureManager::getSingleton().setDefaultNumMipmaps(5);
	ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

	return true;
}

bool DFACore::DFAGameStateManager::keyClicked(const OIS::KeyEvent &e)
{
	return mStates.back()->keyClicked(e);
}

bool DFACore::DFAGameStateManager::keyPressed(const OIS::KeyEvent &e)
{
	return mStates.back()->keyPressed(e);
}

bool DFACore::DFAGameStateManager::keyReleased(const OIS::KeyEvent &e)
{
	return mStates.back()->keyReleased(e);
}

bool DFACore::DFAGameStateManager::mouseMoved(const OIS::MouseEvent &e)
{
	return mStates.back()->mouseMoved(e);
}

bool DFACore::DFAGameStateManager::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return mStates.back()->mousePressed(e, id);
}

bool DFACore::DFAGameStateManager::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return mStates.back()->mouseReleased(e, id);
}

bool DFACore::DFAGameStateManager::frameStarted(const FrameEvent &evt)
{
	return mStates.back()->frameStarted(evt);
}

bool DFACore::DFAGameStateManager::frameEnded(const FrameEvent &evt)
{
	return mStates.back()->frameEnded(evt);
}

DFACore::DFAGameStateManager* DFACore::DFAGameStateManager::getSingletonPtr(void)
{
	return ms_Singleton;
}

DFACore::DFAGameStateManager& DFACore::DFAGameStateManager::getSingleton(void)
{ 
	assert(ms_Singleton);
	return *ms_Singleton;
}