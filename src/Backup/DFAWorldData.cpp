#include "DFAWorldData.h"

#include "DFAAvatar.h"

using namespace Ogre;

DFAData::DFAWorldData *DFAData::DFAWorldData::mWorldData = 0;

DFAData::DFAWorldData::DFAWorldData()
{
	
}

DFAData::DFAWorldData::~DFAWorldData()
{
	
}

std::list<DFACore::DFAWorldObject*>& DFAData::DFAWorldData::getWorldObjects(void)
{
	return mWorldObjects;
}

DFACore::DFASceneObject* DFAData::DFAWorldData::getSceneObjectFromMesh(Ogre::MovableObject *movableObject)
{
	DFACore::DFASceneObject *sceneObject = NULL;
	std::list<DFACore::DFAWorldObject*>::iterator iter = mWorldObjects.begin();
	for(;iter != mWorldObjects.end(); iter++)
	{
		sceneObject = dynamic_cast<DFACore::DFASceneObject*>((*iter));
		if(sceneObject)
		{
			OgreMax::OgreMaxScene::ObjectExtraDataMap dataMap = sceneObject->getObjectExtraDataMap();
			if(dataMap.find(movableObject) != dataMap.end())
				return sceneObject;
		}
	}

	return sceneObject;
}

DFAData::DFAWorldData* DFAData::DFAWorldData::getSingletonPtr(void)
{
	if(!mWorldData)
	{
		mWorldData = new DFAData::DFAWorldData();
	}

	return mWorldData;
	
	//return ms_Singleton;
}