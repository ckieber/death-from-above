#include "DFAFrameManager.h"
#include "DFAUnguidedMissile.h"
#include "DFAWorldData.h"
#include "OgreOggSound.h"

#define RAYCAST_UPDATE_FRAME 4

#define CAMERA_DIST 5
#define CAMERA_HEIGHT 2
#define CAMERA_LOOK_AT 1

#define MAX_RANGE 2000
#define MAX_SPEED 500
#define DAMAGE 2500

using namespace Ogre;

DFACore::DFAUnguidedMissile::DFAUnguidedMissile(const int id, const String &name, Ogre::SceneManager *sceneManager, const QueryMask queryMask)
{
	mCollisionTool = new DFATools::DFACollisionTool(sceneManager);
	mDestroyed = false;
	mPlayDestroyedAnimation = false;
	mId = id;
	mName = name;
	mQueryMask = queryMask;
	mSceneManager = sceneManager;
	
	mSpeed = MAX_SPEED;
	mDamage = DAMAGE;
	mDirection = Vector3::ZERO;
	mDistanceTraveled = 0;

	//create missile object
	mNamePrefix = StringConverter::toString(id) + "_";
	mMovementNode = sceneManager->getRootSceneNode()->createChildSceneNode(mNamePrefix + "UnguidedMissileMovementNode");
	mObjectNode = mMovementNode->createChildSceneNode(mNamePrefix + "UnguidedMissileNode");
	mCameraNode = mMovementNode->createChildSceneNode(mNamePrefix + "UnguidedMissileCameraNode");
	Entity *missile = sceneManager->createEntity(mNamePrefix + mName, "S-8Rocket.mesh");
	missile->setQueryFlags(queryMask);
	mObjectNode->attachObject(missile);
	mObjectNode->scale(3.5, 3.5, 3.5);

	//set camera node position
	mCameraNode->setPosition(Vector3(0, CAMERA_HEIGHT, -CAMERA_DIST));
	mCameraNode->lookAt(Vector3(0, 0, CAMERA_LOOK_AT), Node::TS_PARENT);
	mCameraNode->roll(Degree(180));

	loadAnimations(sceneManager);
	loadSounds();
}

DFACore::DFAUnguidedMissile::~DFAUnguidedMissile()
{
	if(mObjectNode)
	{
		mObjectNode->detachObject(mNamePrefix + mName);
		mSceneManager->destroyEntity(mNamePrefix + mName);
		mSceneManager->destroySceneNode(mObjectNode);
	}
	if(mCameraNode)
		mSceneManager->destroySceneNode(mCameraNode);
	if(mMovementNode)
		mSceneManager->destroySceneNode(mMovementNode);
	
	if(mCollisionTool)
		delete mCollisionTool;
}

void DFACore::DFAUnguidedMissile::loadAnimations(Ogre::SceneManager *sceneManager)
{
	
}

void DFACore::DFAUnguidedMissile::loadSounds()
{
	
}

void DFACore::DFAUnguidedMissile::updateAnimations(const Ogre::Real frameRate)
{
	
}

void DFACore::DFAUnguidedMissile::fire(const Ogre::Vector3 startPosition, const Ogre::Vector3 targetPosition, const Ogre::Vector3 ownerDirection)
{
	mStartPosition = startPosition;
	mPositionLastRaycast = startPosition;
	mTargetPosition = targetPosition;
	mDirection = targetPosition - startPosition;
	mDirection.normalise();
	setPosition(startPosition);

	//set new direction
	Vector3 currDirection = mMovementNode->getOrientation() * Vector3::NEGATIVE_UNIT_Y;
	Quaternion newOrientation = currDirection.getRotationTo(mDirection);
	mMovementNode->setOrientation(newOrientation);
	/*
	//get the direction
	Quaternion orientation = mMovementNode->getOrientation();
	orientation.normalise();
	Vector3 forward = orientation * Ogre::Vector3(0, 0, 1.0);
	forward.normalise();
	Vector3 right = orientation * Ogre::Vector3(-1.0, 0, 0);
	right.normalise();

	//get horizontal angle in between
	Vector3 forwardHorizontal = forward;
	forwardHorizontal.y = 0;
	Vector3 directionHorizontal = mDirection;
	directionHorizontal.y = 0;
	Real angleYaw = forwardHorizontal.angleBetween(directionHorizontal).valueDegrees();

	//get vertical angle in between
	Vector3 forwardVertical = forward;
	forwardVertical.x = 0;
	forwardVertical.z = 0;
	Vector3 directionVertical = mDirection;
	directionVertical.x = 0;
	directionVertical.z = 0;
	Real anglePitch = forwardVertical.angleBetween(directionVertical).valueDegrees();

	mObjectNode->setOrientation(Quaternion(Degree(anglePitch), right));
	mMovementNode->setOrientation(Quaternion(Degree(angleYaw), Vector3::UNIT_Y));
	*/
}

Vector3 DFACore::DFAUnguidedMissile::getPosition(void)
{
	return mMovementNode->getPosition();
}

Ogre::String DFACore::DFAUnguidedMissile::getNodeName(void)
{
	return mObjectNode->getName();
}

Ogre::SceneNode* DFACore::DFAUnguidedMissile::getCameraNode(void)
{
	return mCameraNode;
}

void DFACore::DFAUnguidedMissile::setPosition(const Ogre::Vector3 position)
{
	mMovementNode->setPosition(position);
}

void DFACore::DFAUnguidedMissile::update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager)
{
	updatePosition(frameRate);
	updateAnimations(frameRate);
}

void DFACore::DFAUnguidedMissile::updatePosition(const Ogre::Real frameRate)
{
	//get old and new position
	const Vector3 oldPosition = getPosition();
	const Real distance = mSpeed * frameRate;
	const Vector3 newPosition = oldPosition + (distance * mDirection);
	mDistanceTraveled += distance;
	
	bool destroy = false;
	Ogre::MovableObject *target;
	Ogre::String hitTarget = "";
	if(DFAData::DFAFrameManager::getSingletonPtr()->isInUpdateFrame(RAYCAST_UPDATE_FRAME))
	{
		const uint32 queryFlags = (~AVATAR & ~WEAPON) & (ENEMY | STATIONARY);
		hitTarget = mCollisionTool->getCollisionTargetType(mPositionLastRaycast, newPosition, target, 0.04, 0, queryFlags);
		mPositionLastRaycast = newPosition;
	}

	if(hitTarget == "SceneObject")
	{
		DFASceneObject *sceneObject = dynamic_cast<DFASceneObject*>(DFAData::DFAWorldData::getSingletonPtr()->getSceneObjectFromMesh(target));
		if(sceneObject)
		{
			//testing position of impact
			ParticleSystem* pSys2 = mSceneManager->createParticleSystem("fountain1",
				"Examples/PurpleFountain");
			SceneNode* fNode = mSceneManager->getRootSceneNode()->createChildSceneNode();
			fNode->setPosition(newPosition);
			fNode->scale(10,10,10);
			fNode->attachObject(pSys2);

			sceneObject->decreaseHealth(mDamage);
			destroy = true;
		}
	}
	else if(hitTarget == "WorldFragment")
		destroy = true;
	else if(mDistanceTraveled > MAX_RANGE)
		destroy = true;

	if(destroy)
	{
		mPlayDestroyedAnimation = true;
		mDestroyed = true;
	}
	else
	{
		setPosition(newPosition);
	}
}

Ogre::String DFACore::DFAUnguidedMissile::getName(void)
{
	return mName;
}

DFACore::QueryMask DFACore::DFAUnguidedMissile::getQueryMask(void)
{
	return mQueryMask;
}

bool DFACore::DFAUnguidedMissile::isDestroyed(void)
{
	return mDestroyed;
}