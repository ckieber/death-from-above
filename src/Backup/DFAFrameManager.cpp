#include "DFAFrameManager.h"

using namespace Ogre;

DFAData::DFAFrameManager *DFAData::DFAFrameManager::mFrameManager = 0;

DFAData::DFAFrameManager::DFAFrameManager()
{
	mCurrentFrameNumber = 1;
	mUpdateFrequency = 1;
}

DFAData::DFAFrameManager::~DFAFrameManager()
{
	
}

void DFAData::DFAFrameManager::setFrameUpdateFrequency(const int updateFrequency)
{
	if(updateFrequency > 0)
		mUpdateFrequency = updateFrequency;
}

bool DFAData::DFAFrameManager::isInUpdateFrame(const int frameNumber)
{
	if(frameNumber > 0)
	{
		if(frameNumber == mCurrentFrameNumber)
			return true;
	}

	return false;
}

void DFAData::DFAFrameManager::update(void)
{
	if(++mCurrentFrameNumber > mUpdateFrequency)
		mCurrentFrameNumber = 1;
}

DFAData::DFAFrameManager* DFAData::DFAFrameManager::getSingletonPtr(void)
{
	if(!mFrameManager)
	{
		mFrameManager = new DFAData::DFAFrameManager();
	}

	return mFrameManager;
}