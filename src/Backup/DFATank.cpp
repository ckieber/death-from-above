#include "DFATank.h"
#include "OgreOggSound.h"

#define CAMERA_DIST 30
#define CAMERA_HEIGHT 10
#define CAMERA_LOOK_AT 20

using namespace Ogre;

DFAAI::DFATank::DFATank()
{
	mHealth = 5000;
	mCurrentWeapon = 0;
	mDestroyed = false;
	mPlayDestroyedAnimation = false;
}

DFAAI::DFATank::~DFATank()
{
	if(mDotSceneNode)
		mDotSceneNode->Destroy();

	if(mCameraNode)
		delete mCameraNode;
	if(mObjectNode)
		delete mObjectNode;
	if(mMovementNode)
		delete mMovementNode;

	if(mCollisionTool)
		delete mCollisionTool;
}

void DFAAI::DFATank::loadAnimations(Ogre::SceneManager *sceneManager)
{
	
}

void DFAAI::DFATank::loadSounds()
{
	
}

void DFAAI::DFATank::updateAnimations(const Ogre::Real frameRate)
{
	
}

void DFAAI::DFATank::createObject(const Ogre::String name, const int id, const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
								  OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager, const DFACore::QueryMask queryMask)
{
	mCollisionTool = new DFATools::DFACollisionTool(sceneManager);
	mId = id;
	mNamePrefix = StringConverter::toString(id) + "_";
	mName = name;
	mQueryMask = queryMask;
	mDotSceneNode = new OgreMax::OgreMaxScene();
	mDotSceneNode->SetNamePrefix(mNamePrefix);
	mDotSceneNode->Load(sceneName, renderWindow, loadOptions, sceneManager, sceneManager->getRootSceneNode()->createChildSceneNode(mNamePrefix + "TankMovementNode")->createChildSceneNode(mNamePrefix + "TankNode"));

	mMovementNode = sceneManager->getSceneNode(mNamePrefix + "TankMovementNode");
	mObjectNode = sceneManager->getSceneNode(mNamePrefix + "TankNode");
	mCameraNode = mMovementNode->createChildSceneNode(mNamePrefix + "TankCameraNode");

	//mMovementNode->setPosition(Vector3(500, 150, 500));

	//set camera node position
	mCameraNode->setPosition(Vector3(0, CAMERA_HEIGHT, -CAMERA_DIST));
	mCameraNode->lookAt(Vector3(0, 0, CAMERA_LOOK_AT), Node::TS_PARENT);
	mCameraNode->roll(Degree(180));

	//for better testing
	//mAvatarNode->yaw(Degree(-45));
	//mTankNode->scale(Vector3(0.08, 0.08, 0.08));

	//Entity *ent = sceneManager->createEntity("Knot", "knot.mesh");
	SceneNode *moveNode = mObjectNode->createChildSceneNode(mNamePrefix + "TankMoveNode");
	moveNode->setPosition(Vector3(0, 0, 100));
	//moveNode->attachObject(ent);
	//moveNode->setScale(0.01, 0.01, 0.01);

	loadAnimations(sceneManager);
	loadSounds();
}

Vector3 DFAAI::DFATank::getPosition(void)
{
	return mMovementNode->getPosition();
}

Ogre::String DFAAI::DFATank::getNodeName(void)
{
	return mObjectNode->getName();
}

Ogre::SceneNode* DFAAI::DFATank::getCameraNode(void)
{
	return mCameraNode;
}

void DFAAI::DFATank::setPosition(const Ogre::Vector3 position)
{
	mMovementNode->setPosition(position);
}

void DFAAI::DFATank::update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager)
{
	updatePosition(frameRate);
	updateAnimations(frameRate);
}

void DFAAI::DFATank::updatePosition(const Ogre::Real frameRate)
{
	
}

OgreMax::OgreMaxScene::ObjectExtraDataMap& DFAAI::DFATank::getObjectExtraDataMap(void)
{
	return mDotSceneNode->GetAllObjectExtraData();
}

Ogre::String DFAAI::DFATank::getName(void)
{
	return mName;
}

DFACore::QueryMask DFAAI::DFATank::getQueryMask(void)
{
	return mQueryMask;
}

bool DFAAI::DFATank::isDestroyed(void)
{
	return mDestroyed;
}

void DFAAI::DFATank::decreaseHealth(const Ogre::Real damage)
{
	mHealth -= damage;
	if(mHealth <= 0)
		mPlayDestroyedAnimation = true;
}