#include <NxOgre.h>

#include "DFALevelCreator.h"
#include "DFAAvatar.h"
#include "DFAPhysicsManager.h"
#include "DFAProjectileManager.h"
#include "DFATank.h"
#include "DFAWorldData.h"
#include "DFAObjectTextDisplay.h"

using namespace Ogre;

DFAData::DFALevelCreator *DFAData::DFALevelCreator::mLevelCreator = 0;

DFAData::DFALevelCreator::~DFALevelCreator()
{
	
}

Ogre::Vector3 DFAData::DFALevelCreator::getLevelOffset(const int levelNumber)
{
	if(levelNumber >= 0 && levelNumber < mLevels.size())
		return mLevels[levelNumber]->terrainOffset;
	else
		return Vector3(0, 0, 0);
}

Ogre::String DFAData::DFALevelCreator::getTerrainNodeName(const int levelNumber)
{
	if(levelNumber >= 0 && levelNumber < mLevels.size())
		return mLevels[levelNumber]->nodeName;
	else
		return "";
}

void DFAData::DFALevelCreator::createLevel(const int levelNumber, Ogre::SceneManager *sceneManager, Ogre::RenderWindow *renderWindow,
												OgreOggSound::OgreOggSoundManager *soundManager, Ogre::Camera *camera)
{
	std::list<DFACore::DFAWorldObject*> &worldObjects = DFAData::DFAWorldData::getSingletonPtr()->getWorldObjects();

	sceneManager->setAmbientLight(ColourValue(0.25, 0.25, 0.25));

	//create the sky with sun
	sceneManager->setSkyDome(true, "Sky", 2, 4, 10000);
	Light *sun = sceneManager->createLight("SunLight");
	sun->setType(Light::LT_DIRECTIONAL);
	Vector3 lightDirection(1, -1, 0);
	lightDirection.normalise();
	sun->setDirection(lightDirection);
	sun->setDiffuseColour(ColourValue::White);
	sun->setSpecularColour(ColourValue::White);

	//create physics world
	DFAData::DFAPhysicsManager::getSingletonPtr()->createWorld(sceneManager, false);

	//creating terrain
	//sceneManager->setWorldGeometry("DesertTerrain.cfg");
	createTerrain(levelNumber, sun, sceneManager);
/*
	String terrainNodeName = "DesertNode";
	OgreMax::OgreMaxScene *desertScene = new OgreMax::OgreMaxScene();
	desertScene->Load(0, "Desert.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneManager, sceneManager->getRootSceneNode()->createChildSceneNode(terrainNodeName));

	Vector3 levelOffset(0, -500, 0);
	Ogre::SceneNode *desertNode = sceneManager->getSceneNode(terrainNodeName);
	desertNode->scale(Vector3(500, 500, 500));
	desertNode->setPosition(levelOffset);
*/
/*
	LevelData *levelData = new LevelData();
	levelData->nodeName = terrainNodeName;
	levelData->terrainOffset = levelOffset;
	mLevels.push_back(levelData);
*/
	//add hangars
	StaticGeometry *staticGeometry = sceneManager->createStaticGeometry("HangarArea");

	OgreMax::OgreMaxScene *hangarScene = new OgreMax::OgreMaxScene();
	hangarScene->SetNamePrefix(StringConverter::toString(0) + "_");
	hangarScene->Load("Hangar.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneManager, sceneManager->getRootSceneNode()->createChildSceneNode("HangarNode_0"));
	Ogre::SceneNode *hangarNode = (SceneNode*)sceneManager->getRootSceneNode()->removeChild("HangarNode_0");
	//hangarNode->setPosition(Vector3(1000, 50, -4900));
	hangarNode->setPosition(Vector3(7790, 0, 10164));
	//hangarNode->setScale(Vector3(0.2, 0.2, 0.2));
	staticGeometry->addSceneNode(hangarNode);

	//hangarScene = new OgreMax::OgreMaxScene();
	hangarScene->SetNamePrefix(StringConverter::toString(1) + "_");
	hangarScene->Load("Hangar.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneManager, sceneManager->getRootSceneNode()->createChildSceneNode("HangarNode_1"));
	hangarNode = (SceneNode*)sceneManager->getRootSceneNode()->removeChild("HangarNode_1");
	//hangarNode->setPosition(Vector3(800, 50, -4900));
	hangarNode->setPosition(Vector3(7870, 0, 10144));
	//hangarNode->setScale(Vector3(0.2, 0.2, 0.2));
	staticGeometry->addSceneNode(hangarNode);

	//build all static objects
	staticGeometry->build();
	

	/*
	SceneNode *node = msceneManager->getRootSceneNode()->createChildSceneNode("camNode1", Vector3(0,0,500));
	node->lookAt(Vector3::ZERO, Node::TS_WORLD);
	node->attachObject(mCamera);
	node->attachObject(mSoundManager->getListener());
	*/

	//add avatar
	DFACore::DFAAvatar *avatar = DFACore::DFAAvatar::getSingletonPtr();
	avatar->createObject("Avatar", 0, "Mi24.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneManager, DFACore::AVATAR);
	//avatar->setStartingPosition(Vector3(900, 80, -4970));
	avatar->setPosition(Vector3(7922, 1, 10174));// -> for .cfg file
	//avatar->setPosition(Vector3(0,0,0));
	avatar->lookAt(Vector3::ZERO);
	avatar->setWingPayload(1, new DFACore::DFAWeaponContainer(sceneManager, DFACore::ROCKET_80MM, 20));
	avatar->setWingPayload(2, new DFACore::DFAWeaponContainer(sceneManager, DFACore::ROCKET_80MM, 20));
	avatar->setWingPayload(3, new DFACore::DFAWeaponContainer(sceneManager, DFACore::AT6_SPIRAL, 2));
	worldObjects.push_back(avatar);

	//add camera
	SceneNode *camNode = avatar->getCameraNode();
	camNode->attachObject(camera);
	camNode->attachObject(soundManager->getListener());

	//add tanks
	DFAAI::DFATank *abramsTank = new DFAAI::DFATank();
	abramsTank->createObject("M1A2 Abrams", 0, "Abrams3.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneManager, DFACore::ENEMY);
	//abramsTank->setStartingPosition(Vector3(0, 100, 100));
	//abramsTank->setStartingPosition(Vector3(11838, 1, 14459));
	abramsTank->setPosition(Vector3(8102, 1, 10274));
	worldObjects.push_back(abramsTank);

	abramsTank = new DFAAI::DFATank();
	abramsTank->createObject("M1A2 Abrams", 1, "Abrams3.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneManager, DFACore::ENEMY);
	//abramsTank->setStartingPosition(Vector3(0, 100, 100));
	//abramsTank->setStartingPosition(Vector3(11838, 1, 14459));
	abramsTank->setPosition(Vector3(8112, 1, 10274));
	worldObjects.push_back(abramsTank);

	abramsTank = new DFAAI::DFATank();
	abramsTank->createObject("M1A2 Abrams", 2, "Abrams3.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneManager, DFACore::ENEMY);
	//abramsTank->setStartingPosition(Vector3(0, 100, 100));
	//abramsTank->setStartingPosition(Vector3(11838, 1, 14459));
	abramsTank->setPosition(Vector3(8122, 1, 10274));
	worldObjects.push_back(abramsTank);

	//load all weapon meshes
	ResourceGroupManager::getSingletonPtr()->createResourceGroup("preload");
	ResourceGroupManager::getSingletonPtr()->declareResource("S-8Rocket.mesh", "Mesh", "preload");
	ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup("preload");
/*
	abramsTank = new DFAAI::DFATank();
	abramsTank->createObject("M1A2 Abrams", 1, "Abrams2.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneManager);
	abramsTank->setStartingPosition(Vector3(0, 100, 200));
	worldObjects.push_back(abramsTank);
*/





	//just random stuff
	/*Light *light = sceneManager->createLight("Light1");
	light->setType(Light::LT_POINT);
	light->setPosition(Vector3(250, 150, 250));
	light->setDiffuseColour(ColourValue::White);
	light->setSpecularColour(ColourValue::White);
	*/

	//just temporary -> need to change
	//std::list<DFACore::DFAWorldObject*> temp;
}

void DFAData::DFALevelCreator::createTerrain(const int levelNumber, Ogre::Light *sunLight, Ogre::SceneManager *sceneManager)
{
	mTerrainGlobalOptions = new Ogre::TerrainGlobalOptions();
	mTerrainGlobalOptions->setMaxPixelError(8);
	mTerrainGlobalOptions->setLightMapDirection(sunLight->getDerivedDirection());
	mTerrainGlobalOptions->setCompositeMapDistance(3000);
	mTerrainGlobalOptions->setCompositeMapAmbient(sceneManager->getAmbientLight());
	mTerrainGlobalOptions->setCompositeMapDiffuse(sunLight->getDiffuseColour());

	mTerrain = new Ogre::Terrain(sceneManager);
    Ogre::Image img;
    img.load("testTerrain.bmp", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

	Ogre::Terrain::ImportData imp;
	imp.inputImage = &img;
	imp.terrainSize = 513;
	imp.worldSize = 30720;
	imp.inputScale = 2520;
	imp.minBatchSize = 33;
	imp.maxBatchSize = 65;

	// textures
	imp.layerList.resize(1);

	imp.layerList[0].worldSize = 30720;
	imp.layerList[0].textureNames.push_back("terrain_texture.jpg");
	//imp.layerList[1].worldSize = 7680;
	//imp.layerList[1].textureNames.push_back("terrain_detail.jpg");
	
	/*imp.layerList[1].worldSize = 100;
	imp.layerList[1].textureNames.push_back("detail3.jpg");
	imp.layerList[1].textureNames.push_back("detail3_normal.jpg");
	imp.layerList[2].worldSize = 100;
	imp.layerList[2].textureNames.push_back("detail4.jpg");
	imp.layerList[2].textureNames.push_back("detail4_normal.jpg");
	*/

	mTerrain->prepare(imp);
	mTerrain->load();
	mTerrain->setPosition(Vector3(15360,-10,15360));
	mTerrain->freeTemporaryResources();

	//create physics terrain
	createPhysicsTerrain(mTerrain->getMaterialName(),
		mTerrain->getHeightData(),
		mTerrain->getSize(),
		mTerrain->getWorldSize(),
		mTerrain->getMinHeight(),
		mTerrain->getMaxHeight(),
		mTerrain->getPosition());
}

void DFAData::DFALevelCreator::createPhysicsTerrain(const Ogre::String &name,
			float *data,
			Ogre::uint16 size,
			Ogre::Real worldSize,
			Ogre::Real minHeight,
			Ogre::Real maxHeight,
			const Ogre::Vector3 &position)
{
	// Create the manual heightfield
	NxOgre::ManualHeightField *mhf = OGRE_NEW_T(NxOgre::ManualHeightField, Ogre::MEMCATEGORY_GENERAL)();
	mhf->begin(size, size);
	Ogre::Real normMin = -32768.0f;
	Ogre::Real normMax = 32767.0f;
	
	// Sample the data to the manual heightfield
	for(int x = 0; x < size; ++x)
	{
		NxOgre::Enums::HeightFieldTesselation tess = NxOgre::Enums::HeightFieldTesselation_NW_SE;
		for(int z = size-1; z >= 0; --z)
		{
			Ogre::Real height = data[(size * z) + x];
			short sample = (short)(((height - minHeight) / (maxHeight - minHeight)) * (normMax - normMin) + normMin);
			mhf->sample(sample, 0, 0, tess);
			if(tess == NxOgre::Enums::HeightFieldTesselation_NE_SW)
				tess = NxOgre::Enums::HeightFieldTesselation_NW_SE;
			else
				tess = NxOgre::Enums::HeightFieldTesselation_NE_SW;
		}
	}

	// Create the actual heightfield
	NxOgre::HeightField *hf = mhf->end(name.c_str());
	Ogre::Real hf_size = worldSize + (worldSize / size);
	Ogre::Real hf_height = (maxHeight - minHeight) / 2.0f;
	Ogre::Real hf_pose_x = position.x - (worldSize / 2.0f);
	Ogre::Real hf_pose_y = position.y + ((maxHeight + minHeight) / 2.0f);
	Ogre::Real hf_pose_z = position.z - (worldSize / 2.0f);
#if NxOgreVersionMajor <= 1 && NxOgreVersionMinor <= 5
	NxOgre::HeightFieldGeometry *hfg = new NxOgre::HeightFieldGeometry(hf, NxOgre::Vec3(hf_size, hf_height, hf_size));
	hfg->setLocalPose(NxOgre::Matrix44(NxOgre::Vec3(hf_pose_x, hf_pose_y, hf_pose_z)));
	DFAData::DFAPhysicsManager::getSingletonPtr()->getScene()->createSceneGeometry(hfg);
#else
	NxOgre::HeightFieldGeometryDescription desc(hf, NxOgre::Vec3(hf_size, hf_height, hf_size));
	DFAData::DFAPhysicsManager::getSingletonPtr()->getScene()->createSceneGeometry(desc, NxOgre::Matrix44(NxOgre::Vec3(hf_pose_x, hf_pose_y, hf_pose_z)));
#endif
	// Free memory
	OGRE_DELETE_T(mhf, ManualHeightField, Ogre::MEMCATEGORY_GENERAL);
}

DFAData::DFALevelCreator* DFAData::DFALevelCreator::getSingletonPtr(void)
{
	if(!mLevelCreator)
	{
		mLevelCreator = new DFAData::DFALevelCreator();
	}

	return mLevelCreator;
	
	//return ms_Singleton;
}