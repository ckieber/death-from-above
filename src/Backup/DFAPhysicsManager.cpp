#include "DFAPhysicsManager.h"

using namespace Ogre;

DFAData::DFAPhysicsManager *DFAData::DFAPhysicsManager::mPhysicsManager = 0;

DFAData::DFAPhysicsManager::DFAPhysicsManager()
{
	mPhysicsTimeController = 0;
	mVisualDebugger = 0;
}

DFAData::DFAPhysicsManager::~DFAPhysicsManager()
{
	
}

void DFAData::DFAPhysicsManager::update(const Real frameRate)
{
	if(mPhysicsTimeController)
		mPhysicsTimeController->advance();

	//updateVisualDebugger();
}

void DFAData::DFAPhysicsManager::updateVisualDebugger(void)
{
	if(mVisualDebugger)
	{
		mVisualDebugger->draw();
		mVisualDebuggerNode->needUpdate();
	}
}

void DFAData::DFAPhysicsManager::createWorld(Ogre::SceneManager *sceneManager, const bool enableDebugger)
{
	//create world
	mPhysicsWorld = NxOgre::World::createWorld();

	//create scene
	NxOgre::SceneDescription sceneDescription;
	sceneDescription.mGravity = NxOgre::Vec3(0, -9.8, 0);
	sceneDescription.mName = "";
	mPhysicsScene = mPhysicsWorld->createScene(sceneDescription);

	//physical scene values
	mPhysicsScene->getMaterial(0)->setStaticFriction(0.5);
	mPhysicsScene->getMaterial(0)->setDynamicFriction(0.5);
	mPhysicsScene->getMaterial(0)->setRestitution(0.1);

	//create time controller
	mPhysicsTimeController = NxOgre::TimeController::getSingleton();

	//create debugger
	mEnableDebugger = enableDebugger;
	mVisualDebugger = mPhysicsWorld->getVisualDebugger();
	mVisualDebuggerRenderable = new OGRE3DRenderable(NxOgre::Enums::RenderableType_VisualDebugger);
	mVisualDebugger->setRenderable(mVisualDebuggerRenderable);
	mVisualDebuggerNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	mVisualDebuggerNode->attachObject(mVisualDebuggerRenderable);
	if(enableDebugger)
		mVisualDebugger->setVisualisationMode(NxOgre::Enums::VisualDebugger_ShowAll);
	else
		mVisualDebugger->setVisualisationMode(NxOgre::Enums::VisualDebugger_ShowNone);

	//setting remote debugger
	//NxOgre::RemoteDebugger *remoteDebugger = mPhysicsWorld->getRemoteDebugger();
	//remoteDebugger->connect();
}

NxOgre::World* DFAData::DFAPhysicsManager::getWorld(void)
{
	return mPhysicsWorld;
}

NxOgre::Scene* DFAData::DFAPhysicsManager::getScene(void)
{
	return mPhysicsScene;
}

void DFAData::DFAPhysicsManager::visualDebugger(const bool activate)
{
	mEnableDebugger = activate;
	if(activate)
		mVisualDebugger->setVisualisationMode(NxOgre::Enums::VisualDebugger_ShowAll);
	else
		mVisualDebugger->setVisualisationMode(NxOgre::Enums::VisualDebugger_ShowNone);
}

bool DFAData::DFAPhysicsManager::isVisualDebuggerActive(void)
{
	return mEnableDebugger;
}

DFAData::DFAPhysicsManager* DFAData::DFAPhysicsManager::getSingletonPtr(void)
{
	if(!mPhysicsManager)
	{
		mPhysicsManager = new DFAData::DFAPhysicsManager();
	}

	return mPhysicsManager;
}