#include "DFAProjectile.h"
#include "DFAUnguidedMissile.h"
#include "DFAWeaponContainer.h"
#include "DFAWorldData.h"

DFACore::DFAWeaponContainer::DFAWeaponContainer(Ogre::SceneManager *sceneManager, const DFACore::WeaponType weaponType, const int ammunition)
{
	mProjectileId = 0;
	mWeaponType = weaponType;
	mAmmunition = ammunition;
	mSceneManager = sceneManager;
}

DFACore::DFAWeaponContainer::DFAWeaponContainer(Ogre::SceneManager *sceneManager, const int projectileId, const WeaponType weaponType, const int ammunition)
{
	mProjectileId = projectileId;
	mWeaponType = weaponType;
	mAmmunition = ammunition;
	mSceneManager = sceneManager;
}

DFACore::DFAWeaponContainer::DFAWeaponContainer(const DFACore::DFAWeaponContainer &weaponContainer)
{
	mWeaponType = weaponContainer.mWeaponType;
	mAmmunition = weaponContainer.mAmmunition;
	mSceneManager = weaponContainer.mSceneManager;
	mProjectileId = weaponContainer.mProjectileId;
}

DFACore::DFAWeaponContainer::~DFAWeaponContainer()
{

}

void DFACore::DFAWeaponContainer::shoot(const int weaponNumber, const Ogre::Vector3 startPosition, const Ogre::Vector3 direction, const Ogre::Vector3 targetPosition)
{
	if(mAmmunition > 0)
	{
		std::list<DFACore::DFAWorldObject*> &worldObjects = DFAData::DFAWorldData::getSingletonPtr()->getWorldObjects();
		
		if(mWeaponType == MAIN_MACHINE_GUN)
		{
			DFACore::DFAProjectile *projectile = new DFACore::DFAProjectile(mSceneManager, mProjectileId, "12.7mm Machine Gun");
			projectile->fire(startPosition, targetPosition);
			worldObjects.push_back(projectile);
		}
		else if(mWeaponType == ROCKET_80MM)
		{
			DFACore::DFAUnguidedMissile *missile = new DFACore::DFAUnguidedMissile((weaponNumber * 100) + mAmmunition, "S-8Rocket", mSceneManager, WEAPON);
			missile->fire(startPosition, targetPosition, direction);
			worldObjects.push_back(missile);
		}

		mAmmunition--;
	}
}

Ogre::String DFACore::DFAWeaponContainer::getWeaponName(void)
{
	if(mWeaponType == MAIN_MACHINE_GUN)
		return "12.7mm Machine Gun";
	else if(mWeaponType == ROCKET_80MM)
		return "80mm Rocket";
	else if(mWeaponType == AT6_SPIRAL)
		return "AT-6 Spiral";
	else
		return "None";
}

DFACore::WeaponType DFACore::DFAWeaponContainer::getWeaponType(void)
{
	return mWeaponType;
}

int DFACore::DFAWeaponContainer::getAmmunition(void)
{
	return mAmmunition;
}