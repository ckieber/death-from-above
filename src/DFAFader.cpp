#include "DFAFader.h"
#include "windows.h"
#include "OgreMaterialManager.h"
#include "OgreOverlayManager.h"
#include "OgreTechnique.h"
#include "OgreBlendMode.h"

using namespace Ogre;

DFATools::DFAFader::DFAFader(const char *OverlayName, const char *MaterialName, DFATools::DFAFaderCallback *instance)
{
	try
	{
		mFadeop = FADE_NONE;
		mAlpha = 0.0;
		mInstance = instance;

		// Get the material by name
		Ogre::ResourcePtr resptr = Ogre::MaterialManager::getSingleton().getByName(MaterialName);
		Ogre::Material *mat = dynamic_cast<Ogre::Material*>(resptr.getPointer());

		Ogre::Technique *tech = mat->getTechnique(0);	// Get the technique
		Ogre::Pass *pass = tech->getPass(0);			// Get the pass
		mTexUnit = pass->getTextureUnitState(0);		// Get the texture_unit state

		// Get the mOverlay
		mOverlay = Ogre::OverlayManager::getSingleton().getByName(OverlayName);
		mOverlay->hide();

	}
	catch(Ogre::Exception e)
	{
		MessageBoxA(NULL, e.getFullDescription().c_str(), "Fader Exception", MB_OK | MB_ICONERROR | MB_TASKMODAL);
	}
	catch(...)
	{
		MessageBoxA(NULL, "An unknown exception has occured while setting up the fader.  Scene fading will not be supported.", "Fader Exception", MB_OK | MB_ICONERROR | MB_TASKMODAL);
	}
}

DFATools::DFAFader::~DFAFader(void)
{
}

void DFATools::DFAFader::startFadeIn(double duration)
{
	if(duration < 0)
		duration = -duration;
	if(duration < 0.000001)
		duration = 1.0;

	mAlpha = 1.0;
	mTotalDur = duration;
	mCurrentDur = duration;
	mFadeop = FADE_IN;
	mOverlay->show();
}

void DFATools::DFAFader::startFadeOut(double duration)
{
	if(duration < 0)
		duration = -duration;
	if(duration < 0.000001)
		duration = 1.0;

	mAlpha = 0.0;
	mTotalDur = duration;
	mCurrentDur = 0.0;
	mFadeop = FADE_OUT;
	mOverlay->show();
}

void DFATools::DFAFader::fade(double timeSinceLastFrame)
{
	if(mFadeop != FADE_NONE && mTexUnit)
	{
		// Set the mAlpha value of the mOverlay
		mTexUnit->setAlphaOperation(LBX_MODULATE, LBS_MANUAL, LBS_TEXTURE, mAlpha);	// Change the mAlpha operation

		// If fading in, decrease the mAlpha until it reaches 0.0
		if(mFadeop == FADE_IN)
		{
			mCurrentDur -= timeSinceLastFrame;
			mAlpha = mCurrentDur / mTotalDur;
			if(mAlpha < 0.0)
			{
				mOverlay->hide();
				mFadeop = FADE_NONE;
				if(mInstance)
					mInstance->fadeInCallback();
			}
		}

		// If fading out, increase the mAlpha until it reaches 1.0
		else if(mFadeop == FADE_OUT)
		{
			mCurrentDur += timeSinceLastFrame;
			mAlpha = mCurrentDur / mTotalDur;
			if(mAlpha > 1.0)
			{
				mFadeop = FADE_NONE;
				if(mInstance)
					mInstance->fadeOutCallback();
			}
		}
	}
}