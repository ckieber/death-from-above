#include "DFAProjectileManager.h"

using namespace Ogre;

DFACore::DFAProjectileManager *DFACore::DFAProjectileManager::mProjectileManager = 0;

DFACore::DFAProjectileManager::DFAProjectileManager()
{
	SceneManager *sceneManager = Root::getSingletonPtr()->getSceneManager("MainSceneManager");
	mBillboardSet = sceneManager->createBillboardSet("ProjectileSet", 5000);
	mBillboardSet->setDefaultDimensions(0.1, 0.1);
	//mBillboardSet->setCullIndividually(true);
	mBillboardSet->setMaterialName("Projectile/HindMainGunProjectile");
	sceneManager->getRootSceneNode()->attachObject(mBillboardSet);
}

DFACore::DFAProjectileManager::~DFAProjectileManager()
{
	
}

/*
Ogre::BillboardChain* DFACore::DFAProjectileManager::getBBChain(void)
{
	return mBillboardSet;
}
*/

void DFACore::DFAProjectileManager::clearAllBillboards(void)
{
	mBillboardSet->clear();
}

Ogre::Billboard* DFACore::DFAProjectileManager::addBillboard(const Ogre::Vector3 &position)
{
	return mBillboardSet->createBillboard(position);
}

void DFACore::DFAProjectileManager::removeBillboard(Ogre::Billboard *billboard)
{
	mBillboardSet->removeBillboard(billboard);
}

DFACore::DFAProjectileManager* DFACore::DFAProjectileManager::getSingletonPtr(void)
{
	if(!mProjectileManager)
	{
		mProjectileManager = new DFACore::DFAProjectileManager();
	}

	return mProjectileManager;
}