#ifndef __DFAMissile__
#define __DFAMissile__

#include "DFACollisionTool.h"
#include "DFAWeapon.h"

namespace DFACore
{
	class DFAMissile : public DFAWeapon
	{
	public:
		DFAMissile(const Ogre::uint32 queryMask) : DFAWeapon(queryMask) { }
		virtual ~DFAMissile() { }

		virtual Ogre::String getNodeName(void) = 0;
		virtual Ogre::Vector3 getPosition(void) = 0;
		virtual void setPosition(const Ogre::Vector3 position) = 0;
		virtual Ogre::SceneNode* getCameraNode(void) = 0;
		virtual Ogre::uint32 getQueryMask(void) = 0;

	protected:
		virtual void updatePosition(const Ogre::Real frameRate) = 0;
		virtual void updateAnimations(const Ogre::Real frameRate) = 0;
		virtual void loadAnimations(Ogre::SceneManager *sceneManager) = 0;
		virtual void loadSounds() = 0;

		DFATools::DFACollisionTool *mCollisionTool;
		Ogre::String mNamePrefix;
		Ogre::Vector3 mPositionLastRaycast;
		Ogre::SceneNode *mCameraNode;
		Ogre::SceneNode *mObjectNode;
		Ogre::SceneNode *mMovementNode;
		Ogre::SceneManager *mSceneManager;
	};
};

#endif