#ifndef __DFAGameStateManager__
#define __DFAGameStateManager__

#include <vector>
#include <Ogre.h>
#include <OgreSingleton.h>

#include "DFAInputManager.h"

namespace DFACore
{
	class DFAGameState;
	class DFAGameStateManager : public Ogre::FrameListener, public OIS::KeyListener, public OIS::MouseListener, public Ogre::Singleton<DFAGameStateManager>
	{
	public:
		DFAGameStateManager();
		~DFAGameStateManager();
		void start(DFACore::DFAGameState *state);
		void changeState(DFACore::DFAGameState *state);
		void pushState(DFACore::DFAGameState *state);
		void popState();
		static DFAGameStateManager& getSingleton(void);
		static DFAGameStateManager* getSingletonPtr(void);
	protected:
		Ogre::Root *mRoot;
		Ogre::RenderWindow *mRenderWindow;
		DFAInput::DFAInputManager *mInputManager;
		OIS::Keyboard *mKeyboard;
		OIS::Mouse *mMouse;

		void setupResources(void);
		bool configure(void);

		bool keyClicked(const OIS::KeyEvent &e);
		bool keyPressed(const OIS::KeyEvent &e);
		bool keyReleased(const OIS::KeyEvent &e);

		bool mouseMoved(const OIS::MouseEvent &e);
		bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted(const Ogre::FrameEvent &evt);
		bool frameEnded(const Ogre::FrameEvent &evt);
	private:
		std::vector<DFACore::DFAGameState*> mStates;
	};
};

#endif