#ifndef __DFAProjectile__
#define __DFAProjectile__

#include "NxOgre.h"
#include "DFAWeapon.h"

namespace DFACore
{
	class DFAProjectile : public DFAWeapon, public NxOgre::Callback
	{
	public:
		void update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager);
		void fire(const Ogre::Vector3 startPosition, const Ogre::Vector3 targetPosition, const Ogre::Vector3 ownerDirection = Ogre::Vector3::ZERO);
		bool isDestroyed(void);
		Ogre::String getName(void);
		NxOgre::RigidBody* getRigidBody(void);

		//physics
		bool onHitEvent(const NxOgre::RaycastHit &hit);
	
		DFAProjectile(Ogre::SceneManager *sceneManager, const int id, const Ogre::String &name, const Ogre::uint32 queryMask = 0xFFFFFFFF);
		~DFAProjectile(void);
	protected:
		DFATools::DFACollisionTool *mCollisionTool;
		Ogre::Billboard *mBillboard;
		Ogre::Real mDistanceToTarget;

		/*
		Ogre::BillboardChain::Element mBBElementHead;
		Ogre::BillboardChain::Element mBBElementTail;
		*/
	};
};

#endif