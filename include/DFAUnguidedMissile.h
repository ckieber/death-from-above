#ifndef __DFAUnguidedMissile__
#define __DFAUnguidedMissile__

#include "DFAMissile.h"
#include "NxOgre.h"

namespace DFACore
{
	class DFAUnguidedMissile : public DFAMissile, public NxOgre::Callback
	{
	public:
		DFAUnguidedMissile(const int id, const Ogre::String &name, Ogre::SceneManager *sceneManager, const Ogre::uint32 queryMask = 0xFFFFFFFF);
		~DFAUnguidedMissile();

		void update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager);
		bool isDestroyed(void);
		void fire(const Ogre::Vector3 startPosition, const Ogre::Vector3 targetPosition, const Ogre::Vector3 ownerDirection);
		Ogre::String getName(void);
		NxOgre::RigidBody* getRigidBody(void);
		Ogre::String getNodeName(void);
		Ogre::Vector3 getPosition(void);
		void setPosition(const Ogre::Vector3 position);
		Ogre::SceneNode* getCameraNode(void);
		Ogre::uint32 getQueryMask(void);
		void setTargetFlags(const Ogre::uint32 targetFlags);

		//physics
		void onContact(const NxOgre::ContactPair &pair);

	protected:
		void updatePosition(const Ogre::Real frameRate);
		void updateAnimations(const Ogre::Real frameRate);
		void loadAnimations(Ogre::SceneManager *sceneManager);
		void loadSounds();

		Ogre::uint32 mTargetFlags;

		//physics
		NxOgre::Actor *mActor;
	};
};

#endif