#ifndef __DFAFader__
#define __DFAFader__

namespace Ogre {
	class TextureUnitState;
	class Overlay;
};

namespace DFATools
{
	class DFAFaderCallback
	{
	public:
		virtual void fadeInCallback() {}
		virtual void fadeOutCallback() {}
	};

	class DFAFader
	{
	public:
		DFAFader(const char *OverlayName, const char *MaterialName, DFAFaderCallback *instance = 0);
		~DFAFader(void);

		void startFadeIn(double duration = 1.0f);
		void startFadeOut(double duration = 1.0f);
		void fade(double timeSinceLastFrame);

	protected:
		double mAlpha;
		double mCurrentDur;
		double mTotalDur;
		DFAFaderCallback *mInstance;
		Ogre::TextureUnitState *mTexUnit;
		Ogre::Overlay *mOverlay;

		enum mFadeop
		{
			FADE_NONE,
			FADE_IN,
			FADE_OUT
		}mFadeop;
	};
};

#endif