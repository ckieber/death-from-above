#ifndef __DFALaserGuidedMissile__
#define __DFALaserGuidedMissile__

#include "DFAMissile.h"
#include "NxOgre.h"

namespace DFACore
{
	class DFALaserGuidedMissile : public DFAMissile, public NxOgre::Callback
	{
	public:
		DFALaserGuidedMissile(const int id, const Ogre::String &name, Ogre::SceneManager *sceneManager, const Ogre::uint32 queryMask = 0xFFFFFFFF);
		~DFALaserGuidedMissile();

		void update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager);
		bool isDestroyed(void);
		void fire(const Ogre::Vector3 startPosition, const Ogre::Vector3 targetPosition, const Ogre::Vector3 ownerDirection);
		Ogre::String getName(void);
		NxOgre::RigidBody* getRigidBody(void);
		Ogre::String getNodeName(void);
		Ogre::Vector3 getPosition(void);
		void setPosition(const Ogre::Vector3 position);
		Ogre::SceneNode* getCameraNode(void);
		Ogre::uint32 getQueryMask(void);
		void setTargetFlags(const Ogre::uint32 targetFlags);

		//physics
		void onContact(const NxOgre::ContactPair &pair);

	protected:
		void updatePosition(const Ogre::Real frameRate);
		void updateAnimations(const Ogre::Real frameRate);
		void loadAnimations(Ogre::SceneManager *sceneManager);
		void loadSounds();

		Ogre::uint32 mTargetFlags;
		Ogre::Real mAngleToTarget;
		Ogre::Real mPitchAngle;
		Ogre::Real mYawAngle;
		Ogre::Real mRollAngle;

		//physics
		NxOgre::Actor *mActor;
	};
};

#endif