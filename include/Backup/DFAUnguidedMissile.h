#ifndef __DFAUnguidedMissile__
#define __DFAUnguidedMissile__

#include "DFAMissile.h"

namespace DFACore
{
	class DFAUnguidedMissile : public DFAMissile
	{
	public:
		DFAUnguidedMissile(const int id, const Ogre::String &name, Ogre::SceneManager *sceneManager, const QueryMask queryMask);
		~DFAUnguidedMissile();

		void update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager);
		bool isDestroyed(void);
		void fire(const Ogre::Vector3 startPosition, const Ogre::Vector3 targetPosition, const Ogre::Vector3 ownerDirection);
		Ogre::String getName(void);
		Ogre::String getNodeName(void);
		Ogre::Vector3 getPosition(void);
		void setPosition(const Ogre::Vector3 position);
		Ogre::SceneNode* getCameraNode(void);
		QueryMask getQueryMask(void);

	protected:
		void updatePosition(const Ogre::Real frameRate);
		void updateAnimations(const Ogre::Real frameRate);
		void loadAnimations(Ogre::SceneManager *sceneManager);
		void loadSounds();
	};
};

#endif