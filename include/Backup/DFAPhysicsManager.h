#ifndef __DFAPhysicsManager__
#define __DFAPhysicsManager__

#include <NxOgre.h>
#include <NxOgreOGRE3D.h>
#include <Ogre.h>

namespace DFAData
{
	class DFAPhysicsManager
	{
	public:
		void update(const Ogre::Real frameRate);
		void updateVisualDebugger(void);
		void createWorld(Ogre::SceneManager *sceneManager, const bool enableDebugger);
		NxOgre::World* getWorld(void);
		NxOgre::Scene* getScene(void);
		void visualDebugger(const bool activate);
		bool isVisualDebuggerActive(void);

		static DFAPhysicsManager* getSingletonPtr(void);
	protected:
		DFAPhysicsManager();
		~DFAPhysicsManager();

		//world variables
		NxOgre::World *mPhysicsWorld;
		NxOgre::Scene *mPhysicsScene;
		NxOgre::TimeController *mPhysicsTimeController;

		//debugger variables
		NxOgre::VisualDebugger *mVisualDebugger;
		OGRE3DRenderable *mVisualDebuggerRenderable;
		Ogre::SceneNode *mVisualDebuggerNode;
		bool mEnableDebugger;
	
	private:
		static DFAPhysicsManager *mPhysicsManager;
	};
};

#endif