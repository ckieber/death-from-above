#ifndef __DFAPlayState__
#define __DFAPlayState__

#include <Ogre.h>

#include "DFAGameState.h"
#include "OgreOggSound.h"
#include "DFAWorldObject.h"
#include "DFAWorldData.h"
#include "DFAAvatar.h"
#include "DFAObjectTextDisplay.h"

namespace DFACore
{
	class DFAPlayState : public DFAGameState
	{
	public:
		void enter();
		void exit();

		void pause();
		void resume();

		bool keyClicked(const OIS::KeyEvent &e);
		bool keyPressed(const OIS::KeyEvent &e);
		bool keyReleased(const OIS::KeyEvent &e);

		bool mouseMoved(const OIS::MouseEvent &e);
		bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted(const Ogre::FrameEvent &evt);
		bool frameEnded(const Ogre::FrameEvent &evt);

		void captureInput(void);
		void processLogic(const Ogre::Real frameRate);

		void createScene();

		static DFAPlayState* getInstance()
		{
			return &mPlayState;
		}
	protected:
		DFAPlayState() { }

		Ogre::Root *mRoot;
		Ogre::SceneManager *mSceneMgr;
		Ogre::Viewport *mViewport;
		Ogre::Camera *mCamera;
		OIS::Keyboard *mKeyboard;
		OIS::Mouse *mMouse;
		OgreOggSound::OgreOggSoundManager *mSoundManager;

		//std::list<DFAWorldObject*> mWorldObjects;
		DFAAvatar *mAvatar;
		DFAData::DFAWorldData *mWorldData;

		bool mDoorPressed;
		bool mGearPressed;
		bool mTargetSystemPressed;
		bool mNextTargetPressed;
		bool mNextWeaponPressed;

		//for debugging
		bool mVisualDebuggerPressed;

		DFATools::DFAObjectTextDisplay *mText;
		bool mExitGame;
	private:
		static DFAPlayState mPlayState;
	};
};
#endif