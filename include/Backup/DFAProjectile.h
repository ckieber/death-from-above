#ifndef __DFAProjectile__
#define __DFAProjectile__

#include "DFAWeapon.h"

namespace DFACore
{
	class DFAProjectile : public DFAWeapon
	{
	public:
		void update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager);
		void fire(const Ogre::Vector3 startPosition, const Ogre::Vector3 targetPosition, const Ogre::Vector3 ownerDirection = Ogre::Vector3::ZERO);
		bool isDestroyed(void);
		Ogre::String getName(void);
	
		DFAProjectile(Ogre::SceneManager *sceneManager, const int id, const Ogre::String &name);
		~DFAProjectile(void);
	protected:
		DFATools::DFACollisionTool *mCollisionTool;
		Ogre::Billboard *mBillboard;

		/*
		Ogre::BillboardChain::Element mBBElementHead;
		Ogre::BillboardChain::Element mBBElementTail;
		*/
	};
};

#endif