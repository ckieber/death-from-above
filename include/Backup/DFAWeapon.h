#ifndef __DFAWeapon__
#define __DFAWeapon__

#include "DFAWorldObject.h"

namespace DFACore
{
	class DFAWeapon : public DFAWorldObject
	{
	public:
		DFAWeapon() { }
		virtual ~DFAWeapon() { }

		virtual void fire(const Ogre::Vector3 startPosition, const Ogre::Vector3 targetPosition, const Ogre::Vector3 ownerDirection) = 0;
	
	protected:
		Ogre::Vector3 mStartPosition;
		Ogre::Vector3 mTargetPosition;
		Ogre::Real mDistanceTraveled;
		Ogre::Real mSpeed;
		Ogre::Real mDamage;
		Ogre::Vector3 mDirection;

	};
};

#endif