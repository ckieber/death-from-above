#ifndef __DFAWorldData__
#define __DFAWorldData__

#include "DFASceneObject.h"
#include "DFAWorldObject.h"
#include "OgreMaxScene.hpp"

namespace DFAData
{
	class DFAWorldData
	{
	public:
		std::list<DFACore::DFAWorldObject*>& getWorldObjects(void);
		DFACore::DFASceneObject* getSceneObjectFromMesh(Ogre::MovableObject *movableObject);

		static DFAWorldData* getSingletonPtr(void);
	private:
		DFAWorldData();
		~DFAWorldData();

		std::list<DFACore::DFAWorldObject*> mWorldObjects;

		static DFAWorldData *mWorldData;
	};
};

#endif