#ifndef __DFAWeaponContainer__
#define __DFAWeaponContainer__

#include "DFAWeapon.h"
#include "Ogre.h"

namespace DFACore
{
	enum WeaponType
	{
		MAIN_MACHINE_GUN,
		ROCKET_80MM,
		AT6_SPIRAL,
		EMPTY
	};

	class DFAWeaponContainer
	{
	public:
		void shoot(const int weaponNumber, const Ogre::Vector3 startPosition, const Ogre::Vector3 direction, const Ogre::Vector3 targetPosition);
		Ogre::String getWeaponName(void);
		WeaponType getWeaponType(void);
		int getAmmunition(void);

		DFAWeaponContainer(Ogre::SceneManager *sceneManager, const WeaponType weaponType, const int ammunition);
		DFAWeaponContainer(Ogre::SceneManager *sceneManager, const int projectileId, const WeaponType weaponType, const int ammunition);
		DFAWeaponContainer(const DFAWeaponContainer &weaponContainer);
		~DFAWeaponContainer();
	protected:
		int mAmmunition;
		WeaponType mWeaponType;
		int mProjectileId;
		Ogre::SceneManager *mSceneManager;
	};
};

#endif