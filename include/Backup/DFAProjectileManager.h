#ifndef __DFAProjectileManager__
#define __DFAProjectileManager__

#include <Ogre.h>

namespace DFACore
{
	class DFAProjectileManager
	{
	public:
		//Ogre::BillboardChain* getBBChain(void);
		Ogre::BillboardSet* getBBSet(void);
		Ogre::Billboard* addBillboard(const Ogre::Vector3 &position);
		void clearAllBillboards(void);
		void removeBillboard(Ogre::Billboard *billboard);

		static DFAProjectileManager* getSingletonPtr(void);
	protected:
		DFAProjectileManager();
		~DFAProjectileManager();

		//Ogre::BillboardChain *mBillboardChain;
		Ogre::BillboardSet *mBillboardSet;
	
	private:
		static DFAProjectileManager *mProjectileManager;
	};
};

#endif