#ifndef __DFATank__
#define __DFATank__

#include <Ogre.h>

#include "DFANPWorldObject.h"

namespace DFAAI
{
	class DFATank : public DFACore::DFANPWorldObject
	{
	public:
		DFATank();
		~DFATank();

		void update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager);

		bool isDestroyed(void);
		void decreaseHealth(const Ogre::Real damage);
		
		Ogre::String getNodeName(void);
		Ogre::Vector3 getPosition(void);
		void setPosition(const Ogre::Vector3 position);
		Ogre::SceneNode* getCameraNode(void);
		Ogre::String getName(void);
		DFACore::QueryMask getQueryMask(void);
		OgreMax::OgreMaxScene::ObjectExtraDataMap& getObjectExtraDataMap(void);

		void createObject(const Ogre::String name, const int id,
			const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
			OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager,
			const DFACore::QueryMask queryMask);

	private:
		void loadAnimations(Ogre::SceneManager *sceneManager);
		void loadSounds();
		void updatePosition(const Ogre::Real frameRate);
		void updateAnimations(const Ogre::Real frameRate);

		//OgreMax::OgreMaxScene *mTankScene;
		//Ogre::SceneNode *mTankNode;
		//Ogre::SceneNode *mMovementNode;
		//Ogre::SceneNode *mCameraNode;
	};
};

#endif