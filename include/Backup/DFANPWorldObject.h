#ifndef __DFANPWorldObject__
#define __DFANPWorldObject__

#include "DFASceneObject.h"
#include "DFAWorldObject.h"

namespace DFACore
{
	class DFANPWorldObject : public DFAWorldObject, public DFASceneObject
	{
	public:
		DFANPWorldObject() { }
		virtual ~DFANPWorldObject() { }

	protected:
		int mCurrentWeapon;
	};
};

#endif