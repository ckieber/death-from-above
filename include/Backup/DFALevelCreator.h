#ifndef __DFALevelCreator__
#define __DFALevelCreator__

#include <Ogre.h>
#include <OgreTerrain.h>

#include "DFAWorldObject.h"
#include "OgreOggSound.h"

namespace DFAData
{
	class DFALevelCreator
	{
	public:
		void createLevel(const int levelNumber, Ogre::SceneManager *sceneManager, Ogre::RenderWindow *renderWindow,
												OgreOggSound::OgreOggSoundManager *soundManager, Ogre::Camera *camera);
		Ogre::Vector3 getLevelOffset(const int levelNumber);
		Ogre::String getTerrainNodeName(const int levelNumber);
		static DFALevelCreator* getSingletonPtr(void);
	private:
		struct LevelData
		{
			Ogre::String nodeName;
			Ogre::Vector3 terrainOffset;
		};
		DFALevelCreator() { };
		~DFALevelCreator();

		void createTerrain(const int levelNumber, Ogre::Light *sunLight, Ogre::SceneManager *sceneManager);
		void createPhysicsTerrain(const Ogre::String &name,
			float *data,
			Ogre::uint16 size,
			Ogre::Real worldSize,
			Ogre::Real minHeight,
			Ogre::Real maxHeight,
			const Ogre::Vector3 &position);

		std::vector<LevelData*> mLevels;
		Ogre::Terrain *mTerrain;
		Ogre::TerrainGlobalOptions *mTerrainGlobalOptions;

		static DFALevelCreator *mLevelCreator;
	};
};

#endif