#ifndef __DFAAvatar__
#define __DFAAvatar__

#include <Ogre.h>
#include <process.h>
//#include <OgreSingleton.h>

#include "DFASceneObject.h"
#include "DFAWeaponContainer.h"
#include "DFAWorldObject.h"

typedef std::multimap<double, DFACore::DFAWorldObject*> TargetList;

namespace DFACore
{
	class DFAAvatar : public DFAWorldObject, public DFASceneObject//, public Ogre::Singleton<DFAAvatar>
	{
	public:
		void update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager);
		void accelerate(bool pressed);
		void brake(bool pressed);
		void moveLeft(bool pressed);
		void moveRight(bool pressed);
		void rotateLeft(bool pressed);
		void rotateRight(bool pressed);
		void moveUp(bool pressed);
		void moveDown(bool pressed);

		void shoot(bool pressed, Ogre::SceneManager *sceneManager);
		void nextWeapon(void);
		void previousWeapon(void);
		void nextTarget(void);
		void previousTarget(void);
		void activateTargetSystem(const bool activate);
		bool isTargetSystemActivated(void);
		void selectWeapon(const int num);
		int getCurrentWeapon(void);
		void updateTargetList(void);

		bool isDestroyed(void);
		void decreaseHealth(const Ogre::Real damage);

		Ogre::String getNodeName(void);
		Ogre::Vector3 getPosition(void);
		double getMaxTargetRange(void);
		void setPosition(const Ogre::Vector3 position);
		void lookAt(const Ogre::Vector3 position);
		void rotate(const Ogre::Degree degrees);
		Ogre::SceneNode* getCameraNode(void);
		Ogre::String getName(void);
		QueryMask getQueryMask(void);
		OgreMax::OgreMaxScene::ObjectExtraDataMap& getObjectExtraDataMap(void);
		Ogre::Quaternion getOrientation(void);
		Ogre::Real getRadarAngle(void);
		void targetFriendlies(const bool targetFriendlies);
		void setWingPayload(const int position, DFAWeaponContainer *weaponContainer);

		void createObject(const Ogre::String name, const int id,
			const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
			OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager,
			const QueryMask queryMask);

		bool isDoorOpen() const;
		bool isGearUp() const;
		void doorsOpen(bool open);
		void gearUp(bool up);

		void calculateHeight(void);

		static DFAAvatar* getSingletonPtr(void);
	private:
		DFAAvatar();
		~DFAAvatar();
		void loadAnimations(Ogre::SceneManager *sceneManager);
		void loadSounds();
		void initializeHUD(void);
		void updatePosition(const Ogre::Real frameRate);
		void updateAnimations(const Ogre::Real frameRate);
		void updateHUD(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager);
		bool projectPos(Ogre::Camera *cam, const Ogre::Vector3 &pos, Ogre::Real &x, Ogre::Real &y);
		static unsigned __stdcall calculateHeightThread(void *param);

		void openDoors();
		void closeDoors();
		void gearUp();
		void gearDown();
		void shoot(Ogre::SceneManager *sceneManager);

		Ogre::Real mHeight;
		Ogre::Real mSpeed;
		Ogre::Real mAnglePitch;
		Ogre::Real mAngleRoll;
		Ogre::Real mAngleYaw;
		Ogre::Real mPitchSpeed;
		Ogre::Real mRollSpeed;
		Ogre::Real mYawSpeed;

		bool mBrake, mAccelerate;
		bool mRollLeft, mRollRight;
		bool mYawLeft, mYawRight;

		bool mDoorsOpen;
		bool mGearUp;

		Ogre::Vector3 mDirection;
		Ogre::Vector3 mClimb;
		std::list<Ogre::AnimationState*> mAnimationStates;

		//OgreMax::OgreMaxScene *mAvatarScene;
		//Ogre::SceneNode *mAvatarNode;
		//Ogre::SceneNode *mMovementNode;
		//Ogre::SceneNode *mCameraNode;

		//HUD
		Ogre::Overlay *mCrosshairOverlay;
		Ogre::Overlay *mCrosshairBoxOverlay;
		Ogre::OverlayElement *mCrosshair;
		Ogre::OverlayElement *mCrosshairBox;
		Ogre::Overlay *mHUDOverlay;
		Ogre::OverlayElement *mSpeedOverlayElement;
		Ogre::OverlayElement *mHeightOverlayElement;
		Ogre::OverlayElement *mTargetNameOverlayElement;
		Ogre::OverlayElement *mWeaponNameOverlayElement;
		Ogre::OverlayElement *mWeaponAmmoOverlayElement;

		TargetList mTargetList;
		bool mShoot;
		bool mTargetSystem;
		bool mShootLeftRight; //right = true
		bool mTargetFriendlies;
		DFACore::DFAWorldObject* mCurrentTarget;
		int mCurrentWeapon;
		DFAWeaponContainer* mWeaponContainers[7]; //0 -> main machine gun; 1-6 -> wing positions

		HANDLE mHeightThreadHandle;
		static DFAAvatar *mAvatar;
	};
};

#endif