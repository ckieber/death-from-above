#ifndef __DFAWorldData__
#define __DFAWorldData__

#include "DFASceneObject.h"
#include "DFAWorldObject.h"
#include "OgreMaxScene.hpp"

namespace DFAData
{
	class DFAWorldData
	{
	public:
		std::list<DFACore::DFAWorldObject*>& getWorldObjects(void);
		DFACore::DFASceneObject* getSceneObjectFromMesh(Ogre::MovableObject *movableObject);
		Ogre::uint32 getUniqueID(void);

		static DFAWorldData* getSingletonPtr(void);
	protected:
		std::list<DFACore::DFAWorldObject*> mWorldObjects;
		Ogre::uint32 mIDCounter;

	private:
		DFAWorldData();
		~DFAWorldData();

		static DFAWorldData *mWorldData;
	};
};

#endif