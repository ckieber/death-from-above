#ifndef __DFAIntroState__
#define __DFAIntroState__

#include <Ogre.h>

#include "DFADshow.h"
#include "DFAGameState.h"

namespace DFACore
{
	class DFAIntroState : public DFAGameState
	{
	public:
		void enter();
		void exit();

		void pause();
		void resume();

		bool keyClicked(const OIS::KeyEvent &e);
		bool keyPressed(const OIS::KeyEvent &e);
		bool keyReleased(const OIS::KeyEvent &e);

		bool mouseMoved(const OIS::MouseEvent &e);
		bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted(const Ogre::FrameEvent &evt);
		bool frameEnded(const Ogre::FrameEvent &evt);

		void captureInput(void);
		void processLogic(const Ogre::Real frameRate);

		void createScene();

		static DFAIntroState* getInstance()
		{
			return &mIntroState;
		}
	protected:
		DFAIntroState() { }

		Ogre::Root *mRoot;
		Ogre::SceneManager *mSceneMgr;
		Ogre::Viewport *mViewport;
		Ogre::Camera *mCamera;
		OIS::Keyboard *mKeyboard;
		OIS::Mouse *mMouse;
		DFAVideo::DirectShowMovieTexture *mDshowMovieTextureSystem;
		bool mExitGame;
	private:
		static DFAIntroState mIntroState;
	};
};

#endif