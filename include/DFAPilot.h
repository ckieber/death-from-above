#ifndef __DFAPilot__
#define __DFAPilot__

#include <CEGUI.h>

namespace DFAData
{
	class DFAPilot
	{
	public:
		enum PilotStatus
		{
			ACTIVE,
			INJURED,
			DEAD
		};

		enum PilotRank
		{
			Junior_Lieutenant,
			Lieutenant,
			Senior_Lieutenant,
			Captain,
			Major,
			Lieutenant_Colonel,
			Colonel,
			Major_General,
			Lieutenant_General,
			Colonel_General,
			General_Of_The_Army
		};

		DFAPilot(void);
		DFAPilot(const CEGUI::String name);

		void resetPilot(void);
		const CEGUI::String getName(void) const;
		void setName(const CEGUI::String name);

		const CEGUI::String getStatus(void) const;
		void setStatus(const PilotStatus status);
		void setStatus(const CEGUI::String status);

		const CEGUI::String getRank(void) const;
		void setRank(const PilotRank rank);
		void setRank(const CEGUI::String rank);
		void promote();
		void demote();

		const int getScore(void) const;
		void setScore(const int score);

		const CEGUI::String getKills(void) const;
		void setKills(const int kills);
		void addKills(const int kills);

		const CEGUI::String getPlayedMissions(void) const;
		const CEGUI::String getCompMissionsTotal(void) const;
		const CEGUI::String getIncompMissionsTotal(void) const;

		const CEGUI::String getCompMissionsDay(void) const;
		void setCompMissionsDay(const int completeMissionsDay);

		const CEGUI::String getCompMissionsNight(void) const;
		void setCompMissionsNight(const int completeMissionsNight);

		const CEGUI::String getIncompMissionsDay(void) const;
		void setIncompMissionsDay(const int incompleteMissionsDay);

		const CEGUI::String getIncompMissionsNight(void) const;
		void setIncompMissionsNight(const int incompleteMissionsNight);

		void getCombatTimeTotal(void) const;
		void getCombatTimeDay(void) const;
		void getCombatTimeNight(void) const;

		void increaseCompMissionsDay(void);
		void increaseCompMissionsNight(void);
		void increaseIncompMissionsDay(void);
		void increaseIncompMissionsNight(void);

		DFAPilot& operator=(const DFAPilot &pilot);
		int operator==(const DFAPilot &pilot);
		int operator<(const DFAPilot &pilot);

	private:
		const CEGUI::String intToString(int integer) const;

		CEGUI::String mName;
		PilotRank mRank;
		PilotStatus mStatus;
		int mScore;
		int mCompleteMissionsNight;
		int mCompleteMissionsDay;
		int mIncompleteMissionsNight;
		int mIncompleteMissionsDay;
		int mCombatTimeDay;
		int mCombatTimeNight;
		int mKills;
	};
};

#endif