#ifndef __DFADshow__
#define __DFADshow__


#define __FILE_UTILSOGREDSHOW_VERSION "11-18-2008a"

#include <Ogre.h>
#include <OgreVector2.h>

namespace DFAVideo
{
	struct DirectShowData;

	/// A class for playing movies in an ogre texture
	class DirectShowMovieTexture
	{
	public:
		// cons / decons
		/// Initializes the dshow object, and creates a texture with the given dimensions.
		/**
		If dontModifyDimensions is false, the system might modify the texture dimensions
		by setting them to the nearest power of two (useful for old computers).
		(Ie, 1024x512 if the original dimensions were 640x480).
		*/
		DirectShowMovieTexture(int width, int height, bool dontModifyDimensions=true);
		/// Destroys the dshow object
		virtual ~DirectShowMovieTexture();

		// basic movie methods
		/// Loads a given movie
		/**
		/param moviePath A string telling the full path of the file to be loaded.
		/param horizontalMirroring A bool telling whether the video should be rendered
		as if seen through a mirror, or not.
		*/
		void loadMovie(const Ogre::String& moviePath, bool horizontalMirroring=false);
		/// Obtains the dimensions of the current movie
		Ogre::Vector2 getMovieDimensions();
		/// Unloads the current movie
		void unloadMovie();

		// methods for movie control
		/// Pauses the current movie
		void pauseMovie();
		/// Starts playing the current movie
		void playMovie();
		/// Makes the current movie rewind
		void rewindMovie();
		/// Stops the current movie
		void stopMovie();
		/// Is the latest video put to play, now playing?
		/** (This is an old implementation of mine; I guess I should re-check this) */
		bool isPlayingMovie();

		// methods on movie texture
		/// Obtain the ogre texture where the movie is rendered
		Ogre::TexturePtr getMovieTexture();
		/// Render a movie frame in the ogre texture
		void updateMovieTexture();
	protected:
		/// Texture where to render the movie
		Ogre::TexturePtr mTexture;
		/// Real texture width
		Ogre::Real mTexWidth;
		/// Real texture height
		Ogre::Real mTexHeight;
		/// Direct Show specific data
		DirectShowData* dsdata;
		/// Do we do horizontal mirroring by software?
		bool mHorizontalMirroring;

		/// Clean the full texture (paint it all black)
		void cleanTextureContents();
	};
};
#endif