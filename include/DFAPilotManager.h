#ifndef __DFAPilotManager__
#define __DFAPilotManager__

#include <list>
#include <fstream>

#include "DFAPilot.h"

#define LOGFILE "PilotLog.log"

namespace DFAData
{
	class DFAPilotManager
	{
	public:
		DFAData::DFAPilot* createPilot(const CEGUI::String &pilotName);
		DFAData::DFAPilot* getPilot(const CEGUI::String &pilotName);
		DFAData::DFAPilot* getActivePilot();
		void setActivePilot(const CEGUI::String &pilotName);
		const CEGUI::String printPilot(const CEGUI::String &pilotName);
		bool exists(const CEGUI::String &pilotName);
		std::list<DFAData::DFAPilot*>* getPilotList(void);
		bool deletePilot(const CEGUI::String &pilotName);
		bool deleteAllPilots(void);

		void saveDataToFile(void);
		void loadDataFromFile(void);

		static DFAPilotManager* getSingletonPtr(void);
	private:
		DFAPilotManager(void) { mActivePilot = NULL; }
		const CEGUI::String encryptString(CEGUI::String input);
		const CEGUI::String encryptInt(int input);

		const CEGUI::String decryptString(CEGUI::String input);
		const int decryptInt(CEGUI::String input);

		DFAData::DFAPilot *mActivePilot;
		std::list<DFAData::DFAPilot*> mPilots;
		static DFAPilotManager *mPilotManager;
	};
};

#endif