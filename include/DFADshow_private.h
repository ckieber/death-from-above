#ifndef __DFADshow_private__
#define __DFADshow_private__

#define __IDxtCompositor_INTERFACE_DEFINED__
#define __IDxtAlphaSetter_INTERFACE_DEFINED__
#define __IDxtJpeg_INTERFACE_DEFINED__
#define __IDxtKey_INTERFACE_DEFINED__

#include <dshow.h>
#include <Qedit.h>// for sample grabber
#include <windows.h>


namespace DFAVideo
{
	struct DirectShowData
	{
		/// Graph object
		IGraphBuilder *pGraph;
		/// Media control object
		IMediaControl *pControl;
		/// Media event object
		IMediaEvent *pEvent;
		/// Grabber filter
		IBaseFilter *pGrabberF;
		/// Grabber object
		ISampleGrabber *pGrabber;
		/// Interface for seeking object
		IMediaSeeking *pSeeking;
		/// Window interface
		/** Useful for some configuration
		*/
		IVideoWindow *pWindow;

		/// Video output width
		int videoWidth;
		/// Video output height
		int videoHeight;
	};

	/// Util function for converting C strings to wide strings
	/** (as needed for path in directshow). */
	WCHAR* util_convertCStringToWString(const char* string);
};
#endif